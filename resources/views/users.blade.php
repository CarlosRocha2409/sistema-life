@extends('layout.base')
@section('icon','fa fa-users')
@section('title', 'Users')
@section('subtitle','Usuarios')
@section('subtitleinfo','usuarios')
@section('content')
<input type="hidden" name="userid" id="userid"/>
<input type="hidden" id="action"/>

<div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Administracion de Usuarios</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
           
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-12">
            <div class="col-md-4">
              <button id="btnNew" type="button" value=""><i class="fa fa-plus"></i></button>
            </div>

          </div>
          <div class="col-md-12" style="padding:15px;"></div>
            <div id="usersGrid"></div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
        <div class="row">
          <div class="col-md-9 col-md-offset-1">
              <div class="hidden">
                <div id='formUser'>
                    
                    <div style="color: white;">
                      <span class="fa fa-search" aria-hidden="true"></span> Administracion de usuario
                    </div>
                    <div id="editUserWindowContent">
                       <form id="userForm">
                  
                        <div id="messagefind"></div>
                        
                        <div class="col-md-6">
                          
                              <div class="col-lg-12">
                                <div class="form-group ">
                                  <label classt=" control-label" for="txtName">Nombre</label>
                                  <input id="txtName"></input> 
                                </div>
                              </div>
                              <div class="col-lg-12">
                                <div class="form-group">
                                  <label class="control-label" for="txtUser">usuario </label>
                                  <input id="txtUser"/>
                                </div>
                              </div>
                              <div class="col-lg-12">
                                <div class="form-group">
                                  <label class="control-label" for="txtMail">correo: </label>
                                  <input id="txtMail"/>
                                </div>
                              </div>
                              <div class="col-lg-12">
                                <div class="form-group">
                                  <label class="control-label" for="txtPassword">contraseña: </label>
                                  <input id="txtPassword" type="password"/>
                                </div>
                              </div>
                              <div class="col-lg-12">
                                <div class="form-group">
                                  <label class="control-label" for="cboPosition">cargo: </label>
                                  <div id="cboPosition"></div>
                                </div>
                              </div>
                              <div class="col-lg-12">
                                <div class="form-group">
                                  <label class="control-label" for="cboRol">rol: </label>
                                  <div id="cboRol"></div>
                                </div>
                              </div>
                        </div>
                        <div class="col-md-6">
                          
                              <div class="col-lg-12">
                                <div class="form-group ">
                                  <label classt=" control-label" for="txtName">direccion</label>
                                  <div id="txtAddress"></div> 
                                </div>
                              </div>
                              <div class="col-lg-12">
                                <div class="form-group">
                                  <label class="control-label" for="txtUser">telefono </label>
                                  <input id="txtPhone"/>
                                </div>
                              </div>
                              <div class="col-lg-12">
                                <div class="form-group">
                                  <label class="control-label" for="txtMail">fecha de Ingreso: </label>
                                  <input id="txtStartDate"/>
                                </div>
                              </div>
                              <div class="col-lg-12">
                                <div class="form-group">
                                  <label class="control-label" for="cboStatus">Estado: </label>
                                  <div id="cboStatus"></div> 
                                </div>
                              </div>
                               <div class="col-lg-12">
                                <div class="form-group">
                                  <label class="control-label" for="image">Imagen: </label>
                                  <input type="file" id="picture" name="picture" accept="image/*"/>
                                  <img id="showlogo" />
                                </div>
                              </div>
                        </div>
                        <div class="col-lg-8 col-lg-offset-2 hidden" id="noteContainer">
                            <div class="form-group">
                              <label class="control-label" for="txtNote">Comentario de baja: </label>
                              <div id="txtNote"></div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                          <div class="col-lg-6 col-lg-offset-5" style="padding-top: 20px;">
                            <div class="form-group">
                              <input type="button" value="Guardar" id="btnSave"></input> 
                            </div>
                          </div>
                        </div>
                  
                      </form>
                    </div>
                </div>
            </div>
            
          </div>
        </div>


@endsection
@section('scripts')
<script type="text/javascript" src="{!! asset('js/jquery.blockUI.js') !!}">
  $(document).ajaxStart(function(){$.blockUI({ message : '<h3>Please Wait...</h3>',css: { 
      border: 'none', 
      padding: '5px',
      'z-index': '2147483647', 
      backgroundColor: '#000', 
      '-webkit-border-radius': '10px', 
      '-moz-border-radius': '10px', 
      opacity: .5, 
      color: '#fff'} 
    });}).ajaxStop(function(){$.unblockUI();});
</script>
<script type="text/javascript" src="{!! asset('js/users.js') !!}"></script>

@endsection