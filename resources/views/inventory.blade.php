@extends('layout.base')
@section('icon','fa fa-inventory')
@section('title', 'Inventario')
@section('subtitle','Inventario')
@section('subtitleinfo','Inventario')
@section('content')
<input type="hidden" name="userid" id="userid"/>
<input type="hidden" id="action"/>

<div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Inventario</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
           
          </div>
        </div>
       
            <form id="inventoryForm">
            <div class="box-body">
              <input type="hidden" id="action" />
              <input type="hidden" id="idProduct" />
              <div class="row">
                <div id="formSection" class="col-md-12 col-lg-12">
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="txtProductCode" class="control-label">Codigo de Producto<sup>*</sup></label>
                      <input type="text" id="txtProductCode" class="form-control"/>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="txtOptionalCode1" class="control-label">Codigo Opcional 1</label>
                      <input type="text" id="txtOptionalCode1" class="form-control"/>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="txtOptionalCode2" class="control-label">Codigo Opcional 2</label>
                      <input type="text" id="txtOptionalCode2" class="form-control"/>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="txtProductName" class="control-label">Nombre de producto<sup>*</sup></label>
                      <input type="text" id="txtProductName" class="form-control"/>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="cboTypeInventory" class="control-label">Categoria<sup>*</sup>
                      @if(session('id_user') == 1)
                        <i id="btnNewCategory" class="fa fa-plus btn btn-link" style="padding:0 !important;"></i>
                      @endif
                      </label>
                      <div id="cboTypeInventory"></div>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group marca hidden">
                      <label for="cboBrand" class="control-label">Marca<sup>*</sup>
                        @if(session('id_user') == 1)
                        <i id="btnNewMarca" class="fa fa-plus btn btn-link" style="padding:0 !important;"></i>
                      @endif
                      </label>
                      <div id="cboBrand"></div>
                  </div>
                  
                  <div class="col-md-4 col-lg-4 form-group unidad_medida hidden">
                      <label for="cboMeasureUnit" class="control-label">Unidad de Medida<sup>*</sup>
                        @if(session('id_user') == 1)
                        <i id="btnNewMeasure" class="fa fa-plus btn btn-link" style="padding:0 !important;"></i>
                        @endif
                      </label>
                      <div id="cboMeasureUnit"></div>
                  </div>
                
                  <div class="col-md-4 col-lg-4 form-group genero hidden">
                      <label for="cboGender" class="control-label">Genero<sup>*</sup>
                        @if(session('id_user') == 1)
                        <i id="btnNewGender" class="fa fa-plus btn btn-link" style="padding:0 !important;"></i>
                        @endif
                      </label>
                      <div id="cboGender"></div>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group talla hidden">
                      <label for="cboSize" class="control-label">Talla<sup>*</sup>
                        @if(session('id_user') == 1)
                        <i id="btnNewSize" class="fa fa-plus btn btn-link" style="padding:0  !important;"></i>
                        @endif
                      </label>
                      <div id="cboSize"></div>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group tipo hidden">
                      <label for="cboColor" class="control-label">Tipo<sup>*</sup>
                        @if(session('id_user') == 1)
                        <i id="btnNewTipo" class="fa fa-plus btn btn-link" style="padding:0 !important;"></i>
                        @endif
                      </label>
                      <div id="cboTipo"></div>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group tela hidden">
                      <label for="cboColor" class="control-label">Tela<sup>*</sup>
                        @if(session('id_user') == 1)
                        <i id="btnNewTela" class="fa fa-plus btn btn-link" style="padding:0 !important;"></i>
                        @endif
                      </label>
                      <div id="cboTela"></div>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group color hidden">
                      <label for="cboColor" class="control-label">Color<sup>*</sup>
                        @if(session('id_user') == 1)
                        <i id="btnNewColor" class="fa fa-plus btn btn-link" style="padding:0 !important;"></i>
                        @endif
                      </label>
                      <div id="cboColor"></div>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group tipo_manga hidden">
                      <label for="cboColor" class="control-label">Tipo de Manga<sup>*</sup>
                        @if(session('id_user') == 1)
                        <i id="btnNewTManga" class="fa fa-plus btn btn-link" style="padding:0 !important;"></i>
                        @endif
                      </label>
                      <div id="cboTManga"></div>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group dimensiones hidden">
                      <label for="cboColor" class="control-label">Dimension:<sup>*</sup>
                        @if(session('id_user') == 1)
                        <i id="btnNewDimension" class="fa fa-plus btn btn-link" style="padding:0 !important;"></i>
                        @endif
                      </label>
                      <div id="cboDimension"></div>
                  </div>
                   <div class="col-md-4 col-lg-4 form-group codigo_proveedor hidden">
                      <label for="txtSerie" class="control-label">Codigo Proveedor:</label>
                      <input type="text" id="txtProviderCode" class="form-control"/>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group serie hidden">
                      <label for="txtSerie" class="control-label">Numero de serie</label>
                      <input type="text" id="txtSerie" class="form-control"/>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="cboStatus" class="control-label">Estado</label>
                      <div id="cboStatus"></div>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="txtDescription" class="control-label">Descripcion</label>
                        <div id="txtDescription"></div>
                  </div>
                  <div class="col-md-4 col-lg-4 ">
                      <label for="photo_asset" class="control-label">Imagen</label>
                      <input type="file" id="photo_asset"/>
                  </div>
                  <div class="col-md-4 col-lg-4 ">
                    <div class="col-md-4 col-md-offset-3 col-lg-4 col-lg-offset-3" style="background-color:#d2d6de;">
                      <img id="preview" alt="" class="img-responsive img-circle" accept="image/*"/>
                    </div>
                  </div>
                </div>
              </div>
              <div id="searchSection" class="row hidden">
                 <div class="col-md-12 col-lg-12">
                    <div id="gridProducts"></div>
                  </div>
              </div>

        </form>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="col-md-12 col-lg-12 pad-4 flex-container ">
            <div>
              @foreach($options as $option)
                @if($option->code_option == 'btnFind')
                  <button type="button" value="Buscar" id='btnFind' ><i class="fa fa-search"></i> Buscar</button>
                @endif
                @if($option->code_option == 'btnNew')
                  <button type="button" value="Nuevo" id='btnNew' ><i class="fa fa-plus"></i> Nuevo</button>
                @endif
                @if($option->code_option == 'btnSave')
                  <button type="button" value="Guardar" id='btnSave'><i class="fa fa-save"></i> Guardar</button>
                @endif
                @if($option->code_option == 'btnDownload')
                  <button type="button" id="btnDownload" ><i class="fa fa-download "></i> Descargar</button>
                @endif
              
              @endforeach
            </div>
          </div>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
        <div class="row">
          <div class="col-md-9 col-md-offset-1">
              <div class="hidden">
                <div id='formCategory'>
                    <input type="hidden" id="categoryTypeAction" value="1"/>
                    <input type="hidden" id="id_category_modal" />
                    <div style="color: white;">
                      <span class="fa fa-cogs" aria-hidden="true"></span> Administrar Categorias
                    </div>
                    <div id="editUserWindowContent">
                      <form id="catForm">
                        <div id="messageModalCategories"></div>
                        <div class="col-md-4 col-md-offset-4" style="margin-top: 10px; margin-bottom: 10px;">
                                <div class="form-group ">
                                  <input id="txtCatName"></input> 
                                  <button id="btnFindCategory" type="button" style="width:'10%';"><i class="fa fa-search"></i></button>
                                </div>
                              </div>
                        <div class="col-md-12">
                          <div class="col-md-4 col-md-offset-2">
                            <div class="form-group">
                              <div class="col-md-10">
                                <label class="control-label" for="txtUser">Marca: </label>
                              </div>
                              <div class="col-md-2">
                                <input id="chkMarca" type="checkbox"/>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10">
                                <label class="control-label" for="txtMail">Genero: </label>
                              </div>
                              <div class="col-md-2">
                                <input id="chkGenero" type="checkbox"/>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10">
                                <label class="control-label" for="txtPassword">Color: </label>
                              </div>
                              <div class="col-md-2">
                                <input id="chkColor" type="checkbox"/>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10">
                                <label class="control-label" for="cboPosition">Talla: </label>
                              </div>
                              <div class="col-md-2">
                                <input id="chkTalla" type="checkbox"/>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10">
                                <label class="control-label" for="cboRol">Tipo: </label>
                              </div>
                              <div class="col-md-2">
                                <input id="chkTipo" type="checkbox"/>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10">
                                <label class="control-label" for="cboRol">Serie: </label>
                              </div>
                              <div class="col-md-2">
                                <input id="chkSerie" type="checkbox"/>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group ">
                              <div class="col-md-10">
                                <label classt=" control-label" for="txtName">Tela:</label>
                              </div>
                              <div class="col-md-2">
                                <input id="chkTela" type="checkbox"/>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10">
                                <label class="control-label" for="txtUser">Tipo de Manga: </label>
                              </div>
                              <div class="col-md-2">
                                <input id="chkTipoManga" type="checkbox"/>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10">
                                <label class="control-label" for="txtMail">Dimensiones: </label>
                              </div>
                              <div class="col-md-2">
                                <input id="chkDimensiones" type="checkbox"/>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10">
                                <label class="control-label" for="cboStatus">Cod. Proveedor: </label>
                              </div>
                              <div class="col-md-2">
                                <input id="chkPCode" type="checkbox"/>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-10">
                                <label class="control-label" for="cboStatus">Und. Medida: </label>
                              </div>
                              <div class="col-md-2">
                                <input id="chkUMedida" type="checkbox"/>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-12" style="margin-top: 30px;">
                          <div class="col-lg-6 col-lg-offset-5" style="padding-top: 20px;">
                            <div class="form-group">
                              <input type="button" value="Guardar" id="btnSaveCategory"></input> 
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-9 col-md-offset-1">
              <div class="hidden">
                <div id='multiForm'>
                    <div style="color: white;">
                      <span id="multiModalTitle" class="fa fa-cogs" aria-hidden="true"></span>
                    </div>
                    <div id="editUserWindowContent">
                      <form id="MultiPForm">
                        <div id="messageModalCategories"></div>
                        <div class="col-md-12 ">
                          <div class="form-group">
                            <div class="col-md-4 col-md-offset-4" style="margin-top: 30px;">
                              <label id="lblMulti" for="txtMulti"></label>
                              <input id="txtMulti" />
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-12" style="margin-top: 30px;">
                          <div class="col-lg-6 col-lg-offset-5" style="padding-top: 20px;">
                            <div class="form-group">
                              <input type="button" value="Guardar" id="btnSaveMultiModal"></input> 
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                </div>
            </div>
          </div>
        </div>


@endsection
@section('scripts')

<script type="text/javascript" src="{!! asset('js/fixedasset.js') !!}"></script>

@endsection