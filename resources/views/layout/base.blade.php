<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title') | Life Nicaragua</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" type="text/css" href="{{ asset( 'css/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-confirm.css') }}" />
  <link rel="stylesheet" type="text/css" href="{{ asset('css/AdminLTE.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/styles/jqx.base.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/styles/jqx.ui-start.css')}}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/site.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/skins/_all-skins.min.css')}}">

  @yield('css')
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
    .RedCell{
      background-color: #d9534f;
      color: white;
    }
  </style>
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-blue fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="home" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Life</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Life </b>Nicaragua</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              {{--<span class="label label-warning">10</span>--}}
            </a>
            {{--<ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>--}}
          </li>

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="img/none.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">{{session('username')}}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="img/none.jpg" class="img-circle" alt="User Image">

                <p>
                  {{session('name')}}
                   {{Auth::User()->username}}
                  {{-- <small>Member since Nov. 2012</small> --}}

                </p>
              </li>
              <!-- Menu Body -->
              {{-- <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>--}}
              <!-- Menu Footer-->
              <li class="user-footer">
                {{-- <div class="pull-left">
                  <a href="backtotickets" class="btn btn-default btn-flat">Back to NOC Tickets</a>
                </div> --}}
                <div class="pull-right">
                  <a href="logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

 @include('layout.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="@yield('icon')"></i>
        @yield('subtitle')
        <small>
        @yield('subtitleinfo')
        </small>
      </h1>
      {{--<ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Layout</a></li>
        <li class="active">Fixed</li>
      </ol>--}}
    </section>

    <!-- Main content down here are the messages box hidden by default-->
    <section class="content">
      <div id="jqxLoader" style="position: absolute;"></div>
      <div id="infobox" class="callout callout-info" hidden></div>
      <div id="warningbox" class="callout callout-warning" hidden ></div>
      <div id="errorbox" class="callout callout-danger" hidden ></div>
      <div id="Bill_Token_Modal" class="hidden">
        <div style="color: white;"><span class="fa fa-money" aria-hidden="true"></span> Generate Token For Credit Bill</div>
        <div id="editUserWindowContent" style="overflow-x:hidden;">
                 <div id="divcopy" class="row pad-1">
                      <div class="col-lg-11 col-md-10 col-sm-10 col-xs-10 nopad-right">
                          <input id="txtToken" class="form-control" readonly="readonly" />
                      </div>
                      <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 nopad-left">
                          <button type="button" id="btncopy" class="btn default"><i class="fa fa-files-o"></i></button>
                      </div>
                 </div>
                 <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-6 col-xs-offset-3">
                      <input class="form-control" id="txtTokenEmail" type="email" placeholder="Input Seller Email"/>
                    </div>
                 </div>

                 <div class="row">
                    <div class="offset-center pad-6">
                      <button id="btnGenToken" class="btn btn-warning">Generate Token</button>
                    </div>
                 </div>



           </div>
           <br/><br/>
        </div>
      <!-- Default box content section-->
      <div id="correctNotification"><label></label></div>
      <div id="wrongNotification"><label></label></div>
      <div class="row">
          <div id="containtmsg" class="col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10 text-center"></div>
      </div>
      @yield('content')
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div id="snackbar"></div>
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.1
    </div>
    <strong>Copyright &copy; 2020 <a href="http://almsaeedstudio.com"></a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul id="tab-options" class="nav nav-tabs nav-justified control-sidebar-tabs">

    </ul>
    <div id="tabContent" class="tab-content">
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script type="text/javascript"  src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script type="text/javascript"  src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script type="text/javascript"  src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script type="text/javascript"  src="{{ asset('js/fastclick.min.js') }}"></script>
<!-- AdminLTE App -->
<script type="text/javascript"  src="{{ asset('js/app.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script type="text/javascript"  src="{{ asset('js/demo.js') }}"></script>

<script type="text/javascript"  src="{{ asset('js/jquery-confirm.js') }}"></script>
<script type="text/javascript"  src="{{ asset('js/jqwidgets-ver4.1.2/jqwidgets/jqx-all.js') }}"></script>
<script type="text/javascript"  src="{{ asset('js/jqwidgets-ver4.1.2/jqwidgets/globalization/globalize.js') }}"></script>
<script type="text/javascript"  src="{{ asset('js/token.js') }}"></script>
<script type="text/javascript"  src="{{ asset('js/jquery.blockUI.js') }}"></script>

<script type="text/javascript">
  $.ajaxSetup({
            headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });
  $(document).ajaxStart(function(){
      $.blockUI({ 
        message : '<h3>Please Wait...</h3>',  
        css: { 
            border: 'none', 
            padding: '5px',
            'z-index': '2147483647', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } 
      });
    }).ajaxStop(function(){
      $.unblockUI();
    });
</script>
@yield('scripts')

</body>
</html>