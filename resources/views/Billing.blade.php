@extends('layout.base')
@section('icon','fa fa-shopping-bag ')
@section('title', 'Billing')
@section('subtitle','Facturacion')
@section('subtitleinfo','Facturar')
@section('content')
<div class="box">
        <div class="box-header with-border">

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
             
            </div>
        </div>
        <form id="billingForm">
            <div class="box-body">
              <input type="hidden" id="action" />
              <input type="hidden" id="providerId" />
              <div id="formSection" class="col-md-12">
                <div class="row">
                  <div  class="col-md-12 col-lg-12">
                    <div class="col-md-4 col-lg-4 form-group">
                        <label for="cbocategories" class="control-label">Tipo<sup>*</sup></label>
                        <div id="cboType"></div>
                    </div>
                    <div class="col-md-4 col-lg-4 form-group">
                        <label for="cbobrands" class="control-label">Cliente<sup>*</sup>
                          <i id="btnNewCustomer" class="fa fa-plus btn btn-link" style="padding:0 !important;"></i>
                        </label>
                        <div id="cboCustomer"></div>
                    </div>
                    <div class="col-md-4 col-lg-4 form-group">
                        <label for="cboassetstatus" class="control-label">Proveedor<sup>*</sup></label>
                        <div id="cboProvider"></div>
                    </div>
                  </div>
                </div>
               
                
              <div class="col-md-12 col-lg-12" style="padding-top: 10px;">
                <div id="gridProducts"></div>
              </div>
                <div class="col-md-4 col-md-offset-8" style="padding-top: 10px;">
                  <div class="col-md-4">
                    <label>Subtotal:</label> 
                  </div>
                  <div class="col-md-8">
                      <input type="text" id="txtSubtotal" disabled="disabled"/>
                  </div>
                  <div class="col-md-4">
                    <label>IVA:</label> 
                  </div>
                  <div class="col-md-8">
                      <input type="text" id="txtIVA" disabled="disabled"/>
                  </div>
                  <div class="col-md-4">
                    <label>Total:</label> 
                  </div>
                  <div class="col-md-8">
                      <input type="text" id="txtTotal" disabled="disabled"/>
                  </div>
                </div>
              </div> 
               <div id="searchSection" class="row hidden">
                 <div class="col-md-12 col-lg-12">
                    <div id="gridBills"></div>
                  </div>
              </div>
             
        </form>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="col-md-12 col-lg-12 pad-4 flex-container ">
                <input type="button" value="Nuevo" id='btnNew' />
                <input type="button" value="Buscar" id='btnFind' />
                <input type="button" value="Guardar" id='btnSave' />
                <input type="button" value="Agregar" id='btnAdd' />
          </div>
        </div>
        <div class="row">
          <div id="formCustomers" class="hidden">
            <div style="color: white;">
              <span class="fa fa-users" aria-hidden="true"></span> Crear Cliente
            </div>
            <div id="editUserWindowContent">
              <form id="custForm">
                <div id="messageModalCustomers"></div>
                <div class="col-md-12" style="padding-top: 20px;">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label" for="txtUser">Codigo del Cliente: </label>
                      <input id="txtCustomerCode" type="text" class="form-control"  />
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label" for="txtUser">Nombre del Cliente: </label>
                      <input id="txtCustomerName" type="text" class="form-control"  />
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label" for="txtUser">Identificacion: </label>
                      <input id="txtCustomerNoID" type="text" class="form-control"  />
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label" for="txtUser">Correo Electronico: </label>
                      <input id="txtCustomerEmail" type="text" class="form-control"  />
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label" for="txtUser">Telefono: </label>
                      <input id="txtCustomerTel" type="text" class="form-control"  />
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label" for="txtUser">Celular: </label>
                      <input id="txtCustomerCel" type="text" class="form-control"  />
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label" for="txtUser">Celular 2: </label>
                      <input id="txtCustomerCel2" type="text" class="form-control"  />
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label" for="txtUser">Direccion: </label>
                      <textarea id="txtCustomerAddress" class="form-control" rows="3" ></textarea> 
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <hr class="my-4">
                    <div class="col-lg-6 col-lg-offset-5" style="padding-top: 20px;">
                      <div class="form-group">
                        <input type="button" value="Guardar" id="btnSaveCustomer"></input> 
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
                <div class="row">
          <div id="formProductAdd" class="hidden">
            <div style="color: white;">
              <span class="fa fa-shopping-bag" aria-hidden="true"></span> Agregar Producto
            </div>
            <div id="editUserWindowContent">
              <form id="prdForm">
                <div id="messageModaladd"></div>
                <div class="col-md-12" style="padding-top: 20px;">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label" for="txtUser">Producto: </label>
                      <div id="cboProductAdd"></div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label" for="txtUser">Servicio: </label>
                      <div id="cboServiceAdd"></div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label" for="txtUser">Cantidad: </label>
                      <input id="txtQt" type="text" class="form-control"  />
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label" for="txtUser">Precio: </label>
                      <input id="txtPrice" type="text" class="form-control"  />
                    </div>
                  </div>
                  
                  <div class="col-lg-12">
                    <hr class="my-4">
                    <div class="col-md-12 flex-container" style="padding-top: 20px;">
                        <input type="button" value="Guardar" id="btnAddProduct"/> 
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /.box-footer-->
</div>
<!-- /.box -->

@endsection
@section('scripts')
<script type="text/javascript" src="{!! asset('js/billing.js') !!}"></script>
<script type="text/javascript"  src="{!! asset('js/bootstrap-filestyle.js') !!}"></script>
@endsection