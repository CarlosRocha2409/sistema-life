@extends('layout.base')
@section('icon','fa fa-briefcase ')
@section('title', 'Providers')
@section('subtitle','Crear Proveedor')
@section('subtitleinfo','Crear Proveedor')
@section('content')
<div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><sup>fields with * are Required</sup></h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
             
            </div>
        </div>
        <form id="providersForm">
            <div class="box-body">
              <input type="hidden" id="action" />
              <input type="hidden" id="providerId" />
              <div id="formSection" class="col-md-12">
                <div class="row">
                  <div  class="col-md-12 col-lg-12">
                    <div class="col-md-4 col-lg-4 form-group">
                        <label for="cbocategories" class="control-label">Codigo Proveedor<sup>*</sup></label>
                        <input type="text" id="txtCode" class="form-control"/>
                    </div>
                    <div class="col-md-4 col-lg-4 form-group">
                        <label for="cbobrands" class="control-label">Nombre Proveedor<sup>*</sup></label>
                        <input type="text" id="txtName" class="form-control"/>
                    </div>
                    <div class="col-md-4 col-lg-4 form-group">
                        <label for="cboassetstatus" class="control-label">Nombre Comercial<sup>*</sup></label>
                        <input type="text" id="txtCommercialName" class="form-control"/>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12 col-lg-12">
                    <div class="col-md-4 col-lg-4 form-group">
                        <label for="cboaclass" class="control-label">identificacion<sup>*</sup></label>
                        <input type="text" id="txtId" class="form-control"/>
                    </div>
                    <div class="col-md-4 col-lg-4 form-group">
                        <label for="cboaclassif" class="control-label">Telefono<sup>*</sup></label>
                        <input type="text" id="txtTelephone" class="form-control"/>
                    </div>
                    <div class="col-md-4 col-lg-4 form-group">
                        <label for="code_asset" class="control-label">celular<sup>*</sup></label>
                        <input type="text" id="txtCellphone" class="form-control"/>
                    </div>
                    
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12 col-lg-12">
                    <div class="col-md-4 col-lg-4 form-group">
                        <label for="name_asset" class="control-label">correo electronico<sup>*</sup></label>
                        <input type="text" id="txtEmail" class="form-control"/>
                    </div>
                    <div class="col-md-4 col-lg-4 form-group">
                        <label for="cboStatus" class="control-label">Estado</label>
                        <div id="cboStatus"></div>
                    </div>
                    <div class="col-md-4 col-lg-4 form-group">
                        <label for="initial_value" class="control-label">Direccion<sup>*</sup></label>
                        <div id="txtAddress" ></div>
                    </div>
                  </div>
                </div>
                
              </div> 

               <div id="searchSection" class="row hidden">
                 <div class="col-md-12 col-lg-12">
                    <div id="gridProviders"></div>
                  </div>
              </div>
             
        </form>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="col-md-12 col-lg-12 pad-4 flex-container ">
                <input type="button" value="Nuevo" id='btnNew'></input>
                <input type="button" value="Buscar" id='btnFind' ></input>
                <input type="button" value="Guardar" id='btnSave' ></input>
          </div>
        </div>
        <!-- /.box-footer-->
</div>
<!-- /.box -->

@endsection
@section('scripts')
<script type="text/javascript" src="{!! asset('js/providers.js') !!}"></script>
<script type="text/javascript"  src="{!! asset('js/bootstrap-filestyle.js') !!}"></script>
@endsection