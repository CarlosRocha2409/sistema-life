@extends('layout.base')
@section('icon','fa fa-briefcase ')
@section('title', 'Fixed Asset')
@section('subtitle','Create Fixed Asset')
@section('subtitleinfo','Create new asset')
@section('content')
<div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><sup>fields with * are Required</sup></h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
             
            </div>
        </div>
        <form id="asset_form">
            <div class="box-body">
              <input type="hidden" id="action" />
              <input type="hidden" id="idasset" />
              <div class="row">
                  <div class="col-md-12 col-lg-12">
                      <div class="col-md-12 col-lg-12">
                        <div class="col-md-6 col-lg-6">
                          <div id="assetinfo" class="form-group-sm col-lg-12 col-md-12 hidden">
                            <label class="badge hov" id="assetstate"></label><br/>
                            <label class="badge hov bwarning" id="assetdeprecatedvalue"></label>
                          </div>
                        </div>
                        <div id="assigned-div" class="col-lg-6 col-md-6 hidden">
                            <span>
                              <label class="badge hov pull-right" id="assigned-label"></label>
                              <i id="assigned-icon" class="fa fa-user-circle fa-2 pull-right"></i>
                            </span>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-lg-12">
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="cbocategories" class="control-label">Asset Category<sup>*</sup></label>
                      <div id="cbocategories"></div>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="cbobrands" class="control-label">Asset Brand<sup>*</sup></label>
                      <div id="cbobrands"></div>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="cboassetstatus" class="control-label">Asset Status<sup>*</sup></label>
                      <div id="cboassetstatus"></div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12 col-lg-12">
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="cboaclass" class="control-label">Asset Class<sup>*</sup></label>
                      <div id="cboaclass"></div>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="cboaclassif" class="control-label">Asset Classification<sup>*</sup></label>
                      <div id="cboaclassif"></div>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="code_asset" class="control-label">Asset Code<sup>*</sup></label>
                      <input type="text" id="code_asset" class="form-control"/>
                  </div>
                  
                </div>
              </div>

              <div class="row">
                <div class="col-md-12 col-lg-12">
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="name_asset" class="control-label">Asset Name<sup>*</sup></label>
                      <input type="text" id="name_asset" class="form-control"/>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="initial_value" class="control-label">Initial Value<sup>*</sup> C$</label>
                      <input type="text" id="initial_value" class="form-control"/>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="solved_value" class="control-label">Solved Value C$</label>
                      <input type="text" id="solved_value" class="form-control"/>
                  </div>
                                    
                  
                </div>
              </div>


              <div class="row">
                <div class="col-md-12 col-lg-12">
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="actual_value" class="control-label">Acutal Value C$</label>
                      <input type="text" id="actual_value" class="form-control"/>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="bill_number" class="control-label">Bill Number</label>
                      <input type="text" id="bill_number" class="form-control"/>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="buy_date" class="control-label">Buy Date<sup>*</sup></label>
                      <div id="buy_date"></div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12 col-lg-12">
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="code_optional1" class="control-label">Optional Code 1</label>
                      <input type="text" id="code_optional1" class="form-control"/>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="code_optional2" class="control-label">Optional Code 2</label>
                      <input type="text" id="code_optional2" class="form-control"/>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="location" class="control-label">Asset Location</label>
                      <input id="location" type="text"/>
                  </div>
                  
                </div>
              </div>

              <div class="row">
                <div class="col-md-12 col-lg-12">
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="solved_value" class="control-label">Asset  Description</label>
                        <div id="description"></div>
                  </div>
                  <div class="col-md-4 col-lg-4 ">
                      <label for="photo_asset" class="control-label">Asset Photo</label>
                      <img id="preview" alt="" class="img-responsive" accept="image/*"/>
                        <input type="file" id="photo_asset"/>
                  </div>
                  <div class="col-md-4 col-lg-4 form-group">
                      <label for="deprecable" class="control-label">Deprecable</label>
                      <div id="deprecable"></div>
                  </div>
                </div>
              </div>

        </form>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <div class="center-block">
          <div class="col-md-12 col-md-offset-1 col-lg-12 col-lg-offset-1 pad-4">
            @foreach($options as $option)
              @if($option->code_option == 'btnfind')
                <input type="button" value="Find Asset" id='btnfindasset' ></input>
              @endif
              @if($option->code_option == 'btndismiss')
                <input type="button" value="Dismiss Asset" id='btndismissasset'></input> 
              @endif
              @if($option->code_option == 'btnassign')
                <input type="button" value="Assing Asset" id='btnassingasset'></input> 
              @endif
              @if($option->code_option == 'btnunassign')
                <input type="button" id="btnunassign" value="Unassign Asset"></input>
              @endif
              @if($option->code_option == 'btnsave')
                <input type="button" value="Save Asset" id='saveasset'></input>
              @endif
              @if($option->code_option == 'btndeprecate')
                <input type="button" value="Run Deprecation" id='deprecate'></input>
              @endif
              @if($option->code_option == 'btndownload')
                <input type="button" id="download" value="Download List"></input>
              @endif
              @if($option->code_option == 'btnnewasset')
                <input type="button" value="New Asset" id='btnnewasset' ></input>
              @endif
              
            @endforeach
          </div>
          </div>
        </div>
        <!-- /.box-footer-->
</div>
<!-- /.box -->
      <div class="hidden">
          <div id='formfind'>
              <div style="color: white;">
                <span class="fa fa-search" aria-hidden="true"></span> Find Fixed Asset
              </div>
              <div id="editUserWindowContent">
            
                  <div id="messagefind"></div>
                  
                  <div class="col-lg-6 col-lg-offset-3">
                    
                        <div class="col-lg-12">
                          <div class="form-group ">
                            <label class=" control-label" for="txtcodeasset">Asset Code </label>
                            <input id="txtcodeasset"></input> 
                          </div>
                        </div>
                        <div class="col-lg-12">
                          <div class="form-group">
                            <label class="control-label" for="dateinitial">Start Date: </label>
                            <div id="dateinitial"></div>
                          </div>
                        </div>
                        <div class="col-lg-12">
                          <div class="form-group">
                            <label class="control-label" for="dateend">End Date: </label>
                            <div id="dateend"></div>
                          </div>
                        </div>
                        <div class="col-lg-12">
                          <div class="col-lg-12 col-lg-offset-3">
                            <div class="form-group">
                              <input type="button" value="Find Ticket" id="btnfind"></input> 
                            </div>
                          </div>
                        </div>
                  </div>
                  <div class="col-md-12 col-lg-12s">
                    <div id="gridfindasset"></div>
                  </div>
            
              </div>
          </div>
      </div>
      <div class="hidden">
          <div id='formassign'>
          <div style="color: white;">
            <span class="fa fa-user-plus" aria-hidden="true"></span> Assign Asset
          </div>
          <div id="editUserWindowContent" style="overflow: hidden;">
        
            <div id="messageassign">
            </div>
            <div class="col-lg-8 col-lg-offset-2 pad-4">
              <div class="row">
                <div class="col-md-12 col-lg-12">
                  <div class="col-lg-12 col-md-12 pad-4">
                    <div class="form-group">
                      <label class="control-label" for="cboemployees">Select Employee</label>
                      <div id="cboemployees"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-lg-12">
                  <div class="col-lg-12 col-lg-offset-3 col-md-12 col-md-offset-3">
                    <div class="form-group">
                      <input type="button" value="Assign Asset" id='asignar'></input> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
        
          </div>
        </div>
      </div>

      <div class="hidden">
          <div id='formdepreciate'>
            <div style="color: white;">
             <span class="fa fa-level-down" aria-hidden="true"></span> Run Monthly Depreciation Process
            </div>
            <div id="editUserWindowContent" style="overflow: hidden;">
        
            <div id="messagedeprecation">
            </div>
            <div class="col-lg-8 col-lg-offset-2 pad-4">
              <div class="row">
                <div class="col-md-12 col-lg-12">
                  <div class="col-lg-12 col-md-12 pad-4">
                    <div class="form-group">
                      <label class="control-label" for="cbomonths">Select Month</label>
                      <div id="cbomonths"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-lg-12">
                  <div class="col-lg-12 col-lg-offset-3 col-md-12 col-md-offset-3">
                    <div class="form-group">
                      <input type="button" value="Run Process" id='run'></input> 
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 col-lg-12">
                  <div class="col-lg-12 col-lg-offset-3 col-md-12 col-md-offset-3">
                    <div class="form-group">
                      <input type="button" value="Downolad Detail" id='btndowndeprecation'></input> 
                    </div>
                  </div>
                </div>
              </div>

            </div>
        
          </div>
        </div>
      </div>
<div id="assetHidden" class="box hidden">
    <div class="box-header with-border">
      <h3 class="box-title">Asset History</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
        <div id="gridAssetHistory"></div>

    </div>
    <!-- /.box-body -->
    <div class="box-footer">
    </div>
    <!-- /.box-footer-->
</div>      
@endsection
@section('scripts')
<script type="text/javascript" src="{!! asset('js/fixedasset.js') !!}"></script>
<script type="text/javascript"  src="{!! asset('js/bootstrap-filestyle.js') !!}"></script>
@endsection