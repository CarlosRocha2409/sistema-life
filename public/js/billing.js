window.prdData=[];
window.cant=0;
$(document).ready(function() 
{	
	getProviders();
	getProducts();
	getCustomers();
	getBills();
	var down=null,assigned=null;
	var categories = [];
	var multiUrl="";
	$("#action").val(1);
	$("#correctNotification").jqxNotification
	({ 
		autoOpen: false, 
		closeOnClick: true, 
		autoClose: true, 
		template: "success",
		theme: "ui-start",
		appendContainer: "#containtmsg",
		width: "100%"
	});
	
    $("#wrongNotification").jqxNotification
	({ 
		autoOpen: false, 
		closeOnClick: true, 
		autoClose: true, 
		template: "error",
		theme: "ui-start",
		appendContainer: "#containtmsg",
		width: "100%"
	});
	$('#formCustomers').jqxWindow({
        autoOpen: false,
        resizable: true,
        width: (window.innerWidth*0.6),
        maxWidth:window.innerWidth,
        isModal: true,
        height: (window.innerHeight*0.7),
        maxHeight:window.innerHeight,
        theme: 'ui-start',
        draggable: true,
        initContent: function ()
        {
			//$('#listboxticketsc').jqxWindow('focus');
		}
	 });
		$('#formProductAdd').jqxWindow({
        autoOpen: false,
        resizable: true,
        width: (window.innerWidth*0.4),
        maxWidth:window.innerWidth,
        isModal: true,
        height: (window.innerHeight*0.5),
        maxHeight:window.innerHeight,
        theme: 'ui-start',
        draggable: true,
        initContent: function ()
        {
			//$('#listboxticketsc').jqxWindow('focus');
		}
	 });
	if($("#btnFind").length >0)
	{
		$("#btnFind").jqxButton({ width: 120, height: 30, theme: 'ui-start'});
	}
	if($("#btnSave").length > 0)
		$("#btnSave").jqxButton({ width: '105', height: '30', theme: 'ui-start'});
	if($("#btnNew").length > 0)
	{
		$("#btnNew").jqxButton({ width: '105', height: '30', theme: 'ui-start'});
	}
	if($("#btnAdd").length > 0)
	{
		$("#btnAdd").jqxButton({ width: '105', height: '30', theme: 'ui-start'});
	}
	$("#btnSaveCustomer").jqxButton({ width: '105', height: '30', theme: 'ui-start'});
	$("#btnAddProduct").jqxButton({ width: '105', height: '30', theme: 'ui-start'});
	//$("#run").jqxButton({ width: '120', height: '30', theme: 'ui-start'});
	$("#cboType").jqxComboBox({ source: [{id_type:4, type: "Cotizacion"}, {id_type:5, type:"Factura"}], width: '90%', height: 25, theme: "ui-start", promptText: "Seleccionar tipo de documento", displayMember: 'type', valueMember: 'id_type', selectedIndex:0});
	$("#cboCustomer").jqxComboBox({ source:[], width: '90%', height: 25, theme: "ui-start", promptText: "Seleccionar Cliente", displayMember: 'nombre_cliente', valueMember: 'id_cliente', selectedIndex:-1});
	
	$("#cboProvider").jqxComboBox({ source:[],  width: '90%', height: 25, theme: "ui-start", promptText: "Seleccionar Proveedor", displayMember: 'nombre_comercial', valueMember: 'id_proveedor', selectedIndex:-1});
	$("#cboProductAdd").jqxComboBox({ source:[],  width: '100%', height: 25, theme: "ui-start", promptText: "Seleccionar producto", displayMember: 'product_name', valueMember: 'id_product', selectedIndex:-1});
	$("#cboServiceAdd").jqxComboBox({ source:[],  width: '100%', height: 25, theme: "ui-start", promptText: "Seleccionar servicio", displayMember: 'servicio', valueMember: 'id_servicio', selectedIndex:-1});

	$("#txtCustomerCode").jqxInput({height: 25, width: "100%", theme: 'ui-start' });
	$("#txtCustomerName").jqxInput({height: 25, width: "100%", theme: 'ui-start' });
	$("#txtCustomerNoID").jqxInput({height: 25, width: "100%", theme: 'ui-start' });
	$("#txtCustomerEmail").jqxInput({height: 25, width: "100%", theme: 'ui-start' });
	$("#txtCustomerTel").jqxInput({height: 25, width: "100%", theme: 'ui-start' });
	$("#txtCustomerCel").jqxInput({height: 25, width: "100%", theme: 'ui-start' });
	$("#txtCustomerCel2").jqxInput({height: 25, width: "100%", theme: 'ui-start' });
	$("#txtQt").jqxInput({height: 25, width: "100%", theme: 'ui-start' });
	$("#txtPrice").jqxInput({height: 25, width: "100%", theme: 'ui-start' });
	$("#photo_asset").change(function(){
	    readURL(this);
	});
	function isNumberKey(evt)
	{
	    if (event.shiftKey == true) {
        return false;
    }
    // Allow Only: keyboard 0-9, numpad 0-9, backspace, tab, left arrow, right arrow, delete
    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) {
        // Allow normal operation
        return true;
    } else {
        // Prevent the rest
        return false;
    }
	}
	$("#txtCustomerTel, #txtCustomerCel, #txtCustomerCel2, #txtQt, #txtPrice").on("keyup", function(event){
		event.preventDefault();
		event.stopPropagation();
		if (event.shiftKey == true) {
        return false;
    }
    // Allow Only: keyboard 0-9, numpad 0-9, backspace, tab, left arrow, right arrow, delete
    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) {
        // Allow normal operation
        return true;
    } else {
        // Prevent the rest
        return false;
    }
	});
	function getProviders(){
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "/getProviders",
			cache: false,
			success: function(datos)
			{
				$("#cboProvider").jqxComboBox('source', datos);
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener los proveedores', "containtmsg");
				top();
			}
		});
	}
	function getCustomers(){
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "/getCustomers",
			cache: false,
			success: function(datos)
			{
				$("#cboCustomer").jqxComboBox('source', datos);
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener los clientes', "containtmsg");
				top();
			}
		});
	}
	function fetchProducts(){
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "/getProducts",
			cache: false,
			success: function(datos)
			{
				$("#cboProductAdd").jqxComboBox('source', datos);
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener los productos', "containtmsg");
				top();
			}
		});
	}
	function fetchServices(){
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "/getServices",
			cache: false,
			success: function(datos)
			{
				$("#cboServiceAdd").jqxComboBox('source', datos);
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener los servicios', "containtmsg");
				top();
			}
		});
	}
	$('#billingForm').jqxValidator({
	  theme: 'ui-start',
	  animation: 'fade',
	  position: 'topcenter',
	  animationDuration: 1000,
	  rules: 
	  [
	   	{input: '#cboCustomer', message: 'Campo Requerido!', action: 'select, keyup, blur', rule:  
	   		function (input, commit) 
	   		{ 
	     		if($("#cboCustomer").jqxComboBox('getSelectedIndex') == -1)
		     	{ 
		     		return false; 
		     	}
		     	else 
		     	{
		     		return true; 
		     	}
	   		}
	    },
	    {input: '#cboProvider', message: 'Campo Requerido!', action: 'select, keyup, blur', rule:  
	   		function (input, commit) 
	   		{ 
	     		if($("#cboProvider").jqxComboBox('getSelectedIndex') == -1)
		     	{ 
		     		return false; 
		     	}
		     	else 
		     	{
		     		return true; 
		     	}
	   		}
	    },
	    {input: '#cboType', message: 'Campo Requerido!', action: 'select, keyup, blur', rule:  
	   		function (input, commit) 
	   		{ 
	     		if($("#cboType").jqxComboBox('getSelectedIndex') == -1)
		     	{ 
		     		return false; 
		     	}
		     	else 
		     	{
		     		return true; 
		     	}
	   		}
	    },
	   
	  ]
	});
	$('#billingForm').on('validationError', function (event)
	{
		setTimeout(function(){ top(); }, 1000);
		
	});
	$("#btnSaveCustomer").click(function(){
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "/saveCustomerBilling",
			cache: false,
			data:{codigo_cliente:$("#txtCustomerCode").val(), nombre_cliente:$("#txtCustomerName").val(), no_id:$("#txtCustomerNoID").val(), direccion:$("#txtCustomerAddress").val(), correo:$("#txtCustomerEmail").val(), telefono:$("#txtCustomerTel").val(), celular:$("#txtCustomerCel").val(), celular2:$("#txtCustomerCel2").val() },
			success: function(datos)
			{
				$("#formCustomers").jqxWindow("close");
				getCustomers();
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al crear el cliente', "messageModalCustomers");
				top();
			}
		});
	});
	$('#billingForm').on('validationSuccess', function (event)
	{
		$.confirm({
			icon: 'fa fa-floppy-o',
			title: 'Confirmar',
			content: 'Quieres guardar esta Facctura/Cotizacion?',
			type: 'blue',
			typeAnimated: true,
		    closeIcon: true,
		    closeIconClass: 'fa fa-close',
		    draggable: true,
		    animation: 'zoom',
			closeAnimation: 'scale',
			animationBounce: 2,
			animationSpeed: 300,
			buttons:{
					Si:{
							btnClass: 'btn-danger',
							keys: ['enter', 'y','Y'],
							action: function()
							{
								if(parseInt($("#action").val()) == 1){
									save();
								}
								else if(parseInt($("#action").val()) == 2){
									update();
								}
								this.close();
							}
						},
					No:{
							btnClass: 'btn-info',
							keys: ['esc','n','N'],
							action: function()
							{
								return;
							}
						}
			
					},
			
		});
							
						
 	});
		$("#cbomonths").change(function(){
		if($("#cbomonths").val())
		{
			$("#btndowndeprecation").show(1000);
		}
		else
		{
			$("#btndowndeprecation").hide(1000);
		}
	});
	$("#download").click(function(){
		window.open('downloadassetList');
	});
	$("#btndowndeprecation").click(function(){
		var month= $("#cbomonths").val();
		var year= $("#cboyears").val();
		$("#cbomonths").jqxComboBox("clearSelection");
		window.open('downloaddepreciationdetail?month='+ month+'&year='+year);
	});
	$("#btnAddProduct").click(function(){
		var prd = $("#cboProductAdd").jqxComboBox('getSelectedItem');
		var service = $("#cboServiceAdd").jqxComboBox('getSelectedItem');
		let cmp = window.prdData.filter(data => parseInt(data.id_product) == parseInt(prd.value));
		if(cmp.length > 0){
			ShowNotificacionError('El producto seleccionado ya fue agregado', "messageModaladd");
			clearFormAdd();
			return;
		}
		console.log(prd, service);
		window.prdData.push({id_product:prd.value, product_name:prd.label, id_service: service.value, service:service.label, quantity:parseFloat($("#txtQt").val()), price:parseFloat($("#txtPrice").val()), iva:((parseFloat($("#txtPrice").val())*parseFloat($("#txtQt").val()))*0.15), total:((parseFloat($("#txtPrice").val())*parseFloat($("#txtQt").val()))*1.15) })
		getProducts();
		calc();
		clearFormAdd();
		$("#formProductAdd").jqxWindow("close");
		//$("#gridProducts").jqxGrid('refreshdata');
	});
	$("#btnSave").click(function(){

		$('#billingForm').jqxValidator('validate');
		
		
	});
	$("#btnFind").click(function(){
		$("#searchSection").removeClass("hidden");
		$("#formSection").addClass("hidden");
		clearForm();
	});
	$("#btnNew").click(function(){
		$("#searchSection").addClass("hidden");
		$("#formSection").removeClass("hidden");
		clearForm();
	});
	function save(){
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "/saveBill",
			cache: false,
			data: { id_customer: $("#cboCustomer").val(), id_proveedor: $("#cboProvider").val(), id_type: $("#cboType").val(), subtotal: $("#txtSubtotal").val(), iva: $("#txtIVA").val(), total: $("#txtTotal").val(), cantidad:window.cant, detail: window.prdData},
			success: function(datos)
			{
				if(datos)
				{
					if(datos.resultado == "OK")
					{
						$("#action").val(1);
						getBills();
						$("#btnFind").trigger("click");
					}
					else
					{
						ShowNotificacionError("Error de Insercion", "containtmsg");
						top();
					}
				}				
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al insertar el Proveedor', "containtmsg");
				top();
			}
		});
	}
   	function update(){
   		ShowNotificacionError('ERROR', "containtmsg");return;
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "/updateProvider",
			cache: false,
			data: { id_proveedor: $("#providerId").val(),  codigo_proveedor: $("#txtCode").val(), nombre: $("#txtName").val(), nombre_comercial: $("#txtCommercialName").val(), identificacion: $("#txtId").val(), correo:$("#txtEmail").val(), id_status:$("#cboStatus").val(), telefono: $("#txtTelephone").val(), celular: $("#txtCellphone").val(), direccion:$("#txtAddress").val()},
			success: function(datos)
			{
				if(datos)
				{
					if(datos.resultado == "OK")
					{
						$("#action").val(1);
						getProviders();
						$("#btnFind").trigger("click");
					}
					else
					{
						ShowNotificacionError("Error de Insercion", "containtmsg");
						top();
					}
				}				
			},
			error: function(datos)
			{
				ShowNotificacionError('ERROR', "containtmsg");
				top();
			}
		});
	}
	function getDetail(id){
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "/getDetails",
			cache: false,
			data: { id_documento: id},
			success: function(datos)
			{
				if(datos)
				{
					window.prdData = datos;
					getProducts();
				}				
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error', "containtmsg");
				top();
			}
		});
	}
	$("#btnDownload").click(function(){
		window.open("/downloadProducts","_blank", "width=10 height=10");
	});
	
	$("#btnAdd").click(function(){
		fetchServices();
		fetchProducts();
		$("#formProductAdd").removeClass("hidden");
		$("#formProductAdd").jqxWindow("open");
	});
	$("#btnNewCustomer").click(function(){
		$("#formCustomers").removeClass("hidden");
		$("#formCustomers").jqxWindow("open");

	});
	function calc(){
		let sub=0, iva=0, tot=0;
		window.prdData.forEach(data =>{
			sub+=parseFloat(data.price)*parseFloat(data.quantity);
			iva+=parseFloat(data.iva);
			tot+=parseFloat(data.total);
			window.cant+=parseFloat(data.quantity);
		});
		$("#txtSubtotal").val(sub);
		$("#txtIVA").val(iva);
		$("#txtTotal").val(tot);
	}
	function getBills()
	{

		var source =
		{
		    datatype: "json",
		    datafields: [
		    { name: 'id_documento' },
		        { name: 'codigo_documento' },
		        { name: 'fecha' },
		        { name: 'date_created'},
		        { name: 'cliente' },
		        { name: 'id_cliente' },
		        { name: 'total_items' },
		        { name: 'subtotal' },
		        { name: 'total_impuesto' },
		        { name: 'total_neto' }
		    ],
		    method:"GET",
		    url: '/getBills',
			cache: false
		    
		};
		var dataAdapter = new $.jqx.dataAdapter(source);
		$("#gridBills").jqxGrid(
        {
			width: '100%',
			source: dataAdapter,
			theme: 'ui-start',
			autoheight: "true",
			columnsresize: true,
			selectionmode: 'singlerow',
			enabletooltips: true,
			sortable: true,
			editable: false,
			enablebrowserselection: true,
			pagesize: 20,
		 	altrows: true,
			columns: [
	
				  { text: 'Codigo', datafield: 'codigo_documento', displayfield: 'codigo_documento', width: "15%"},
				  { text: 'Cliente', datafield: 'cliente', displayfield: 'cliente', width: "25%"},
				  { text: 'Cantidad', datafield: 'total_items', displayfield: 'total_items', width: "10%"},
				  { text: 'Subtotal', datafield: 'subtotal', displayfield: 'subtotal', width: "10%"},
				  { text: 'IVA', datafield: 'total_impuesto', displayfield: 'total_impuesto', width: "10%"},
				  { text: 'Total', datafield: 'total_neto', displayfield: 'total_neto', width: "10%"},
				  { text: 'Fecha', datafield: 'date_created', displayfield: 'date_created', width: "20%"  },

			]
		});
	}
		function getProducts()
	{

		var source =
		{
		    datatype: "json",
		    datafields: [
		    	
		        { name: 'id_product' },
		        { name: 'product_name' },
		        { name: 'id_service'},
		        { name: 'service' },
		        { name: 'quantity' },
		        { name: 'price' },
		        { name: 'iva' },
		        { name: 'total' }
		    ],
		    localdata: window.prdData
		};
		var dataAdapter = new $.jqx.dataAdapter(source);
		$("#gridProducts").jqxGrid(
        {
			width: '100%',
			source: dataAdapter,
			theme: 'ui-start',
			autoheight: "true",
			columnsresize: true,
			selectionmode: 'singlerow',
			enabletooltips: true,
			sortable: true,
			editable: false,
			enablebrowserselection: true,
			pagesize: 20,
		 	altrows: true,
			columns: [
	
				  { text: 'Nombre', datafield: 'product_name', displayfield: 'product_name', width: "20%"},
				  { text: 'Servicio Aplicado', datafield: 'service', displayfield: 'service', width: "20%"},
				  { text: 'Cantidad', datafield: 'quantity', displayfield: 'quantity', width: "12%"},
				  { text: 'Precio', datafield: 'price', displayfield: 'price', width: "12%"},
				  { text: 'IVA', datafield: 'iva', displayfield: 'iva', width: "12%"},
				  { text: 'Total', datafield: 'total', displayfield: 'total', width: "12%"},
				  { text: 'Eliminar', datafield: 'id_product', displayfield: 'id_product', width: "12%", cellsalign: 'center', cellsrenderer: function(row, columnfield, value, defaulthtml, columnproperties){console.log(row, columnfield, value, defaulthtml, columnproperties);  return '<div class="text-center"><span class="fa fa-trash deletePrd btn" style="font-size:15px; color:#f8d7da;" data-id="'+value+'"></span></div>'} },

			]
		});
	}
	$("body").on("click", ".deletePrd", function(){
		if($("#action").val() == 2){
			ShowNotificacionError("No se puede eliminar un producto de una factura guardada", "containtmsg");
			return;
		}
		var index = 0;
		var prdID=parseInt($(this).data("id"));
		window.prdData.forEach((val, idx)=>{
			if(val.id_product == prdID){
				index=idx;
			}
		});
		window.prdData.splice(index,1);
		getProducts();
		calc();
		
	});
	$('#gridBills').on('rowdoubleclick', function (event) 
	{
		clearForm();
		$("#action").val(2);
		$("#cboType").val($('#gridBills').jqxGrid('getrowdata', event.args.rowindex).id_tipo_inv);
		$("#docID").val($('#gridBills').jqxGrid('getrowdata', event.args.rowindex).id_documento);
		$("#cboProvider").val($('#gridBills').jqxGrid('getrowdata', event.args.rowindex).id_proveedor);
		$("#cboCustomer").val($('#gridBills').jqxGrid('getrowdata', event.args.rowindex).id_cliente);
		getDetail($('#gridBills').jqxGrid('getrowdata', event.args.rowindex).id_documento);
		$("#searchSection").addClass("hidden");
		$("#formSection").removeClass("hidden");
	});
	

	function readURL(input) 
	{

	    if (input.files && input.files[0]) 
	    {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#preview').attr('src', e.target.result);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	function top() 
	{
    	//document.body.scrollTop = 0; // For Chrome, Safari and Opera
    	//document.documentElement.scrollTop = 0; // For IE and Firefox
    	$("html, body").animate({ scrollTop: 0 }, 600);
	} 	

	function ShowNotificacionError(texto, modal)
	{
		$("#wrongNotification").jqxNotification({ appendContainer: "#"+modal });
		$("#wrongNotification label").html(texto);
		$("#wrongNotification").jqxNotification("open");
	}

	function ShowNotificacionOk(texto)
	{
		$("#correctNotification label").html(texto);
		$("#correctNotification").jqxNotification("open");
	}
	function clearCategoryModal(){
		$("#id_category_modal").val("");
		$("#categoryTypeAction").val(1);
		$("#txtCatName").val("");
		$("#chkMarca").prop("checked",false); 
		$("#chkGenero").prop("checked",false);
		$("#chkColor").prop("checked",false);
		$("#chkTalla").prop("checked",false); 
		$("#chkTipoManga").prop("checked",false);
		$("#chkDimensiones").prop("checked",false);
		$("#chkPCode").prop("checked",false);
		$("#chkUMedida").prop("checked",false);
		$("#chkSerie").prop("checked",false);

	}
	function clearFormAdd(){
		$("#cboProductAdd").jqxComboBox("selectedIndex", -1);
		$("#cboServiceAdd").jqxComboBox("selectedIndex", -1);
		$("#txtQt").val("");
		$("#txtPrice").val("");

	}
	function clearForm()
	{
		$("#action").val(1);
		$("#docID").val('');
		$("#cboCustomer").val('');
		$("#cboProvider").val('');
		$("#cboType").val(4);
		window.prdData=[];
		setTimeout(function(){ top(); }, 700);

	}

});//end ready event