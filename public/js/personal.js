$(document).ready(function() 
{	
	$("#txtcode").jqxInput({placeHolder: "Enter a Code", height: 30, width: 300, minLength: 1, theme: 'orange' });
	$("#txtname").jqxInput({placeHolder: "Enter a Name", height: 30, width: 300, minLength: 1, theme: 'orange' });
	$("#chkisactive").jqxCheckBox({ width: 300, height: 30, theme: 'orange' });
	$("#typeaccion").val(1);
	
	$('#filephoto').filestyle();
	
	if($("#btnupdate").length >0)
		 $("#btnupdate").jqxButton({ width: '150', height: '30', theme: 'orange'});
	if($("#btnsave").length >0)
		 $("#btnsave").jqxButton({ width: '150', height: '30', theme: 'orange'});
	if($("#btnexport").length >0)
		 $("#btnexport").jqxButton({ width: '150', height: '30', theme: 'orange'});

	$("#btnfindcode").jqxButton({ width: '150', height: '30', theme: 'orange'});
	
	$("#jqxLoader").jqxLoader({ isModal: true, width: 100, height: 60, imagePosition: 'center', theme: 'orange' });
	
	$('#customer_form').jqxValidator(
	{
		theme: 'orange',
		animation: 'fade',
		rules: 
		[
			{ input: '#txtcode', message: 'Code is required!', action: 'keyup, blur', rule: 'required' },
			{ input: '#cboarea', message: 'Area is required!', action: 'select, blur', rule: function (input, commit) 
				{ 
					if($("#cboarea").jqxComboBox('getSelectedIndex') == -1)
					{ return false; }
					else { return true; }
				}
			},
			{ input: '#cboposition', message: 'Position is required!', action: 'select, blur', rule: function (input, commit) 
				{ 
					if($("#cboposition").jqxComboBox('getSelectedIndex') == -1)
					{ return false; }
					else { return true; }
				}
			},
			{ input: '#txtname', message: 'Name is required!', action: 'keyup, blur', rule: 'required' }
		]
	});	
	
	$("#correctNotification").jqxNotification
	({ 
		autoOpen: false, 
		closeOnClick: true, 
		autoClose: true, 
		template: "success",
		theme: "orange",
		appendContainer: "#containtmsg",
		width: "100%"
	});
	
    $("#wrongNotification").jqxNotification
	({ 
		autoOpen: false, 
		closeOnClick: true, 
		autoClose: true, 
		template: "error",
		theme: "orange",
		appendContainer: "#containtmsg",
		width: "100%"
	});
	
	GetAreas();
	GetPosition();
	GetFixedAssets(0);
	LockControls();
	
	function GetAreas()
	{
		var typemacrosSource = 
		{
			dataType: "json",
			type: "POST",
			dataFields: [
				{name: 'id_area'},
				{name: 'area'}
			],
			url: 'GetAreas',
			async: false
		};
		
		typemacrosAdapter = new $.jqx.dataAdapter(typemacrosSource);
	
		$("#cboarea").jqxComboBox(
		{
			source: typemacrosAdapter,	
			width: 300,
			height: 25,
			theme: "orange",
			promptText: "Select Area",
			searchMode: "contains",
			displayMember: 'area',
			valueMember: 'id_area'
		});
	}
	
	function GetPosition()
	{
		var typemacrosSource = 
		{
			dataType: "json",
			type: "POST",
			dataFields: [
				{name: 'id_position'},
				{name: 'position'}
			],
			url: 'GetPosition',
			async: false
		};
		
		typemacrosAdapter = new $.jqx.dataAdapter(typemacrosSource);
	
		$("#cboposition").jqxComboBox(
		{
			source: typemacrosAdapter,	
			width: 300,
			height: 25,
			theme: "orange",
			promptText: "Select Position",
			searchMode: "contains",
			displayMember: 'position',
			valueMember: 'id_position'
		});
	}
	
	function GetFixedAssets(idstaff)
	{
		var source =
        {
			type: "POST",
			datatype: "json",
			data: {idstaff: idstaff},
            datafields: [
				{ name: 'id_asset', type: 'int'},
				{ name: 'code_asset', type: 'string'},
				{ name: 'name_asset', type: 'string'},
				{ name: 'description', type: 'string'},
				{ name: 'is_depreciable', type: 'bool'},
				{ name: 'is_down', type: 'bool'},
				{ name: 'date_asigned', type: 'string'}
			],
            url: 'GetAssetAssigned',
			cache: false,
			async: false
        };
		
		var StaffAdapter = new $.jqx.dataAdapter(source);
		
		$("#gridassets").jqxGrid(
        {
			width: '100%',
			source: StaffAdapter,
			theme: 'orange',
			autoheight: true,
			rowsheight: 25,
			selectionmode: 'singlerow',
			pageable: true,
			enabletooltips: true,
			sortable: true,
			pagermode: "simple",
			enablebrowserselection: true,
			columns: [
                  { text: 'Code Fixed Asset', datafield: 'code_asset', width: 177 },
				  { text: 'Name Asset', datafield: 'name_asset', width: 177},
				  { text: 'Description', datafield: 'description', width: 331},
				  { text: 'Is Depreciable', datafield: 'is_depreciable', columntype: 'checkbox', width: 100},
				  { text: 'Is Down', datafield: 'is_down', columntype: 'checkbox', width: 100},
				  { text: 'Date Assignment', datafield: 'date_asigned', width: 177}
			]
		});
	}
	
	$("#txtcode").keypress(function(event) 
	{	
		if (event.which == 13) 
		{
			$("#btnfindcode").click();
		}
	});
	
	$("#txtcode").keyup(function(event) 
	{	
		if ($("#txtcode").val() == '' || $("#txtcode").val() == undefined)
		{
			Clearform();
			LockControls();
		}
		
		this.value = this.value.toLocaleUpperCase();
	});
	
	$("#btnfindcode").on('click', function ()
	{
		if ($("#txtcode").val() == '' || $("#txtcode").val() == undefined)
		{
			Clearform();
			LockControls();
			ShowNotificacionError('Debe ingresar el Codigo del Empleado a buscar', 'containtmsg');
			return;
		}
		
		$('#jqxLoader').jqxLoader('open');
		Clearform();
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "GetStaffRRHHbyCode",
			async: false,
			data: { codestaff: $("#txtcode").val() },
			success: function(datos)
			{
				if(datos)
				{	
						$("#typeaccion").val(2);
						$("#idstaff").val(datos.id_personal);
						$("#txtname").val(datos.name_complete);
						$("#cboarea").jqxComboBox('selectItem', datos.id_area);
						$("#cboposition").jqxComboBox('selectItem', datos.id_position);
						if (datos.photo_personal != null)
						{
							$("#imgphoto").attr('src', datos.photo_personal).width(150).height(150);
						}
						$("#chkisactive").val(datos.is_active);
						GetFixedAssets($("#idstaff").val());
						LockControls();
						$('#jqxLoader').jqxLoader('close');
				}
				else
				{
					$("#imgphoto").attr('src', '').width(1).height(1);
					$("#filephoto").attr('src', '');
					$("#typeaccion").val(1);					
					UnLockControls();
					$('#jqxLoader').jqxLoader('close');
					ShowNotificacionError('El codigo de Empleado ingresado no existe', 'containtmsg');
					return;
				}
			}
		});
	});
	
	$("#btnupdate").on('click', function ()
	{
		if ($("#txtcode").val() == '' || $("#txtcode").val() == undefined)
		{
			ShowNotificacionError("Debe buscar el Colaborador a actualizar la informacion", "containtmsg");
			return;
		}
		
		UnLockControls();
	});
	
	$("#btnsave").on('click', function ()
	{
		$('#customer_form').jqxValidator('validate');
	});
	
	$('#customer_form').on('validationSuccess', function (event) 
	{
		$.confirm({
			icon: 'fa fa-warning',
			title: 'Confirmation',
			content: 'Are you Sure Save/Update this Staff?',
			type: 'red',
			typeAnimated: true,
			closeIcon: true,
			closeIconClass: 'fa fa-close',
			draggable: true,
			animation: 'zoom',
			closeAnimation: 'scale',
			animationBounce: 2,
			animationSpeed: 300,
			buttons:
			{
				YES:
				{
					btnClass: 'btn-info lock',
					keys: ['enter', 'y','Y'],
					action: function()
					{
						$(".lock").prop('disabled',true);
						this.close();
						SaveStaff();
					}
				},
				NO:{
					btnClass: 'btn-danger',
					keys: ['esc','n','N'],
					action: function()
					{
						return;
					}
				}
    
			},
		});
		
	});
	
	function SaveStaff()
	{
		$('#jqxLoader').jqxLoader('open');
		
		if ($("#typeaccion").val() == 1)
		{
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "SaveStaffRRHH",
				async: false,
				data: { codestaff: $("#txtcode").val(), areastaff: $("#cboarea").val(), positionstaff: $("#cboposition").val(), namestaff: $("#txtname").val(), 
				photostaff: $("#imgphoto").attr('src'), is_active: $("#chkisactive").val() },
				success: function(datos)
				{
					if (datos['respuesta'] == 'OK')
					{
						$("#idstaff").val(datos['resultado']);	
						$("#typeaccion").val(2);
						$("#jqxLoader").jqxLoader('close');
						ShowNotificacionOk("El Colaborador " + $("#txtcode").val() + " ha sido insertado/actualizado", "containtmsg");
						return;
					}
					else
					{
						$("#idstaff").val("");	
						$("#typeaccion").val(1);
						$("#jqxLoader").jqxLoader('close');
						ShowNotificacionError("Error de Inserción/Actualización favor intente nuevamente ", 'containtmsg');
						return;
					}
				}
			});
		}
		else if ($("#typeaccion").val() == 2)
		{
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "UpdateStaffRRHH",
				async: false,
				data: { idstaff: $("#idstaff").val(), codestaff: $("#txtcode").val(), areastaff: $("#cboarea").val(), positionstaff: $("#cboposition").val(), 
				namestaff: $("#txtname").val(), photostaff: $("#imgphoto").attr('src'), is_active: $("#chkisactive").val() },
				success: function(datos)
				{
					if (datos['respuesta'] == 'OK')
					{
						LockControls();
						$("#idstaff").val(datos['resultado']);	
						$("#typeaccion").val(2);
						$("#jqxLoader").jqxLoader('close');
						ShowNotificacionOk("El Colaborador " + $("#txtcode").val() + " ha sido insertado/actualizado", "containtmsg");
						return;
					}
					else
					{
						$("#typeaccion").val(2);
						$("#jqxLoader").jqxLoader('close');
						ShowNotificacionError("Error de Inserción/Actualización favor intente nuevamente ", 'containtmsg');
						return;
					}
				}
			});
		}
		
		$(".lock").prop('disabled',false);
	}
	
	$('#filephoto').on('change', function (event) 
	{		
		LeerURLImage(this);
	});
	
	function LeerURLImage(input)
	{
		if (input.files[0].size >= 1050000)
		{
			$("#imgphoto").val("");
			$("#filephoto").attr('src', '');
			
			ShowNotificacionError('El archivo no puede ser mayor a 1 MB', "containtmsg");
			return
		}
		
		var reader = new FileReader();
		reader.onload = function(e)
		{
			$("#imgphoto").attr('src', e.target.result).width(150);
			$("#imgphoto").attr('src', e.target.result).height(150);
		}
		reader.readAsDataURL(input.files[0]);
	}
	
	$("#btnexport").on('click', function ()
	{
		$('#jqxLoader').jqxLoader('open');
		document.location.href='GetStaffbyExport';
		$('#jqxLoader').jqxLoader('close');
	});
	
});

function ShowNotificacionError(texto, modal)
{
	$("#wrongNotification").jqxNotification({ appendContainer: "#"+modal });
	$("#wrongNotification label").html(texto);
	$("#wrongNotification").jqxNotification("open");
}

function ShowNotificacionOk(texto, modal)
{
	$("#correctNotification").jqxNotification({ appendContainer: "#"+modal });
	$("#correctNotification label").html(texto);
	$("#correctNotification").jqxNotification("open");
}

function Clearform()
{
	$("#typeaccion").val(1);
	$("#imgphoto").attr('src', '').width(1).height(1);
	$("#filephoto").attr('src', '');
	$("#cboarea").jqxComboBox('clearSelection');
	$("#cboposition").jqxComboBox('clearSelection');
	$("#txtname").val("");
	$("#chkisactive").val(0);
	$('#gridassets').jqxGrid('clear');
}

function LockControls()
{
	$("#imgphoto").attr('disabled', 'disabled');
	$("#filephoto").attr('disabled', 'disabled');
	$("#cboarea").jqxComboBox({ disabled: true });
	$("#cboposition").jqxComboBox({ disabled: true });
	$("#txtname").jqxInput({ disabled: true });
	$("#chkisactive").jqxCheckBox({ disabled: true });
}

function UnLockControls()
{
	$("#imgphoto").removeAttr('disabled');
	$("#filephoto").removeAttr('disabled');
	$("#cboarea").jqxComboBox({ disabled: false });
	$("#cboposition").jqxComboBox({ disabled: false });
	$("#txtname").jqxInput({ disabled: false });
	$("#chkisactive").jqxCheckBox({ disabled: false });
}