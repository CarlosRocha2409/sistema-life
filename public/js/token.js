$(document).ready(function() 
{	
	$('#Bill_Token_Modal').jqxWindow({
        autoOpen: false,
        resizable: true,
        width: 520,
        isModal: true,
        height: 200,
        theme: 'orange',
        draggable: false,
        resizable: false,
        initContent: function ()
        {
         //
        }
  });
  $("body").on("click", "#btncopy",function(){
    var token = document.getElementById("txtToken");
    if(!token.value)
    {
      showSnackbar("Primero debe generar el token antes de copiarlo",1);
      return;
    }    
    token.focus();
    token.setSelectionRange(0, token.value.length);
    document.execCommand("Copy");
    showSnackbar("Token Copiado Al Portapapeles");
  });
  function FireToken()
  {
    $('#Bill_Token_Modal').removeClass('hidden');
    $("#txtTokenEmail").val("");
    $("#txtToken").val("");
    $("#Bill_Token_Modal").jqxWindow('open');
  }
  function showSnackbar(message, error=0) 
  {

    var x = document.getElementById("snackbar")
    x.innerHTML=message;
    if(error==1)
    {
      x.style.background="#D9534F";
    }
    else{
      x.style.background="#2196F3";
    }
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  }
  $("body").on("click", "#btnGenToken", function(){
    

    if( $("#txtTokenEmail").val() )
    {
      
      $('#jqxLoader').jqxLoader('open');
      $.ajax({
              type: "GET",
              dataType: "json",
              url: "gentoken",
              cache: false,
              data: { dest_email: $("#txtTokenEmail").val() },
              success: function(datos)
              {
                $('#jqxLoader').jqxLoader('close');
                showSnackbar(datos.respuesta);
                $("#txtToken").val(datos.billing_token);
              },
              error: function(datos)
              {
                $('#jqxLoader').jqxLoader('close');
                ShowNotificacionError('Error de Red!', "containtmsg");
              }
            });       
    }
    else
    {
      showSnackbar("debe introducir un correo valido para enviar el token", 1); 
    }
  });
});