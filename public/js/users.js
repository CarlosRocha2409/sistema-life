
$(document).ready(function() 
{	
	var down=null,assigned=null;
	var start=new Date(), end=new Date();
	start.setHours(0);
	start.setMinutes(0);
	start.setSeconds(0);
	end.setHours(23);
	end.setMinutes(59);
	end.setSeconds(59);
	var cargos = [{id:1, name:"Administrador"},{id:2, name:"Ejecutivo de ventas"}, {id:3, name:"Asistente Administrativo"}, {id:4, name:"Responsable de bodega"}];
	var CARGOSadapter= new $.jqx.dataAdapter(cargos);
	var estados = [{id:1, name:"Activo"}, {id:0, name:"Inactivo"}];
	var estadosAdapter= new $.jqx.dataAdapter(estados);
	$("#action").val(1);
	$("#correctNotification").jqxNotification
	({ 
		autoOpen: false, 
		closeOnClick: true, 
		autoClose: true, 
		template: "success",
		theme: "ui-start",
		appendContainer: "#containtmsg",
		width: "100%"
	});

    $("#wrongNotification").jqxNotification
	({ 
		autoOpen: false, 
		closeOnClick: true, 
		autoClose: true, 
		template: "error",
		theme: "ui-start",
		appendContainer: "#containtmsg",
		width: "100%"
	});
	$('#formUser').jqxWindow({
        autoOpen: false,
        resizable: true,
        width: "100%",
        isModal: true,
        height: 500,
        theme: 'dark',
        draggable: true,
        initContent: function ()
        {
			//$('#listboxticketsc').jqxWindow('focus');
		}
	 });

	getUsers();
	getRoles();	
	$("#txtName").jqxInput({height: 25, width: "100%", theme: 'ui-start', placeHolder: "Nombre del empleado" });
	$("#txtUser").jqxInput({height: 25, width: "100%", theme: 'ui-start', placeHolder: "Uaurios del empleado" });
	$("#txtMail").jqxInput({height: 25, width: "100%", theme: 'ui-start', placeHolder: "Correo del empleado" });
	$("#txtAddress").jqxTextArea({height: 75, width: "100%", theme: 'ui-start', placeHolder: "Direccion del empleado" });
	$("#txtNote").jqxTextArea({height: 75, width: "100%", theme: 'ui-start', placeHolder: "Comentario de baja" });
	$("#txtPhone").jqxFormattedInput({spinButtons: false, radix:"decimal", height: 25, width: "100%", theme: 'ui-start',placeHolder: "Numero de telefono del empleado", max:"99999999"});
	$("#txtStartDate").jqxDateTimeInput({ width: "100%", height: 25,  formatString: 'yyy-MM-dd', theme: 'ui-start', max:(new Date()) });
	$("#txtPassword").jqxPasswordInput({height: 25, width: "100%", theme: 'ui-start', placeHolder: "Contrasena" });
	$("#btnSave").jqxButton({ width: 120, height: 30, theme: 'dark'});
	$("#btnNew").jqxButton({ width: 30, height: 30, theme: 'dark'});
	$("#cboPosition").jqxComboBox({ source:CARGOSadapter,	width:'100%', height:25, theme: "ui-start", promptText: "Seleccione el cargo a asignar", displayMember:"name", valueMember: "id" });
	$("#cboStatus").jqxComboBox({ source:estadosAdapter,	width:'100%', height:25, theme: "ui-start", selectedIndex:0, displayMember:"name", valueMember: "id" });
	function getUsers()
	{
		var source =
	        {
				type: "POST",
				datatype: "json",
				data: {"_token": $('meta[name="csrf-token"]').attr('content')},
	            datafields: [
					{ name: 'id', type: 'integer'},
					{ name: 'name', type: 'String'},
					{ name: 'username', type: 'String'},
					{ name: 'email', type: 'String'},
					{ name: 'direccion', type: 'String'},
					{ name: 'telefono', type: 'String'},
					{ name: 'cargo', type: 'integer'},
					{ name: 'nota_baja', type: 'String'},
					{ name: 'status', type: 'boolean'},
					{ name: 'url_img', type: 'String'},
					{ name: 'fecha_ingreso', type: 'Datetime'},
					
				],
	            url: '/getusers',
				cache: false
	        };
			var cellname = function (row, column, value, data) 
		{
			var val = $('#usersGrid').jqxGrid('getcellvalue', row, "status");
			switch(val)
			{
				case false:
					return "RedCell";
					break;
				
				default:
					break;
			}
		}
			var UsersAdapter = new $.jqx.dataAdapter(source);
			
			$("#usersGrid").jqxGrid(
	        {
				source: UsersAdapter,
				theme: 'ui-start',
				width: '100%',
				rowsheight: 90,
				autoheight: "true",
				columnsresize: true,
				selectionmode: 'singlerow',
				pageable: true,
				enabletooltips: true,
				sortable: true,
				editable: false,
				showfilterrow: true,
            	filterable: true,
				enablebrowserselection: true,
				pagermode: "simple",
				pagesize: 20,
			 	altrows: true,
				columns: [

				 { text: 'Foto', datafield: 'url_img', width: '8%',
        		cellsrenderer: function (row, colum, value) {
                var cell = '<img  class="img-circle" style="margin-left:5px;" src="'+value+'" height="80" />';
                return cell;
		            }},
                { text: 'Nombre', datafield: 'name', displayfield: 'name', width: '20%', cellclassname: cellname},
				{ text: 'Usuario', datafield: 'username', displayfield: 'username', width: '10%', cellclassname: cellname},
				{ text: 'Correo', datafield: 'email', displayfield: 'email', width: '12%', cellclassname: cellname},
				{ text: 'Direccion', datafield: 'direccion', displayfield: 'direccion', width: '20%', cellclassname: cellname},
				{ text: 'Telefono', datafield: 'telefono', displayfield: 'telefono', width: '8%', cellclassname: cellname},
				{ text: 'Fecha De Ingreso', datafield: 'fecha_ingreso', displayfield: 'fecha_ingreso', width: '10%', cellclassname: cellname},
				{ text: 'Cargo', datafield: 'cargo',  width: '10%', cellclassname: cellname, cellsalign: 'center', align: 'center', cellsrenderer: function (row, colum, value) {
                	
                	if(value == 1){

                		return '<div class="jqx-grid-cell-left-align" style="margin-top: 37px;">Administrador</div>';
                	}
                	else{
                		let cell = cargos.filter(function(cargo){
                			if(cargo.id == value){
                				return true;
                			}
                		});
                		return '<div class="jqx-grid-cell-left-align" style="margin-top: 37px;">'+ cell[0].name +'</div>';
                	}}},
                	{ text: 'Estado', datafield: 'status', displayfield: 'status', width: '10%',columntype: 'checkbox', filtertype: 'bool', cellclassname: cellname},
                	{ text: 'Notas', datafield: 'nota_baja', displayfield: 'nota_baja', width: '10%', cellclassname: cellname},
				]
			});	
			
	}

	function top() 
	{
    	//document.body.scrollTop = 0; // For Chrome, Safari and Opera
    	//document.documentElement.scrollTop = 0; // For IE and Firefox
    	$("html, body").animate({ scrollTop: 0 }, 600);
	} 
	function getRoles(){
		var source = 
		{
			dataType: "json",
			type: "GET",
			url: '/getroles',
			
		};
		
		var adapter = new $.jqx.dataAdapter(source);
		
		$("#cboRol").jqxComboBox(
		{
			source: adapter,	
			width: '99%',
			height: 25,
			theme: "ui-start",
			popupZIndex: 99999999,
			multiSelect: true,
			promptText: "Seleccione Rol",
			displayMember: 'rol',
			valueMember: 'id_rol'
		});
	}
$("#cboStatus").on('select', function (event) 
{
	if(event.args.item.value == 0)
	{
		console.log(typeof event.args.item.value);
		$("#noteContainer").removeClass("hidden");
		$("#formUser").jqxWindow({height:600});
	}else{
		$("#noteContainer").addClass("hidden");
	}   

}); 
$('#userForm').jqxValidator(
	 {
	  theme: 'ui-start',
	  animation: 'fade',
	  position: 'topcenter',
	  animationDuration: 1000,
	  rules: 
	  [
	    {input: '#cboPosition', message: 'Debe seccionar un cargo!', action: 'select, keyup, blur', rule:  
	   		function (input, commit) 
	   		{ 
	     		if($("#cboPosition").jqxComboBox('getSelectedIndex') == -1)
		     	{ 
		     		return false; 
		     	}
		     	else 
		     	{
		     		return true; 
		     	}
	   		}
	    },
	    {input: '#cboStatus', message: 'Estado reuquerido!', action: 'select, keyup, blur', rule:  
	   		function (input, commit) 
	   		{ 
	     		if($("#cboStatus").jqxComboBox('getSelectedIndex') == -1)
		     	{ 
		     		return false; 
		     	}
		     	else 
		     	{
		     		return true; 
		     	}
	   		}
	   },
	    {input: '#cboRol', message: 'Rol reuquerido!', action: 'select, keyup, blur', rule:  
	   		function (input, commit) 
	   		{ 
	     		if($("#cboRol").jqxComboBox('getSelectedItems').length == 0)
		     	{ 
		     		return false; 
		     	}
		     	else 
		     	{
		     		return true; 
		     	}
	   		}
	   },


	   	{input: '#txtNote', message: ' campo reuquerido!', action: 'keyup, blur', rule: function (input, commit) 
	   		{ 
	     		if($("#cboStatus").val() == 0 && $("#action").val() == 2  && $("#txtNote").val() == '' )
		     	{ 
		     		return false; 
		     	}
		     	else 
		     	{
		     		return true; 
		     	}
	   		}
	   },
	   	{input: '#txtUser', message: ' campo reuquerido!', action: 'keyup, blur', rule: 'required' },
	   	{input: '#txtName', message: ' campo reuquerido!', action: 'keyup, blur', rule: 'required' },
	   	{input: '#txtMail', message: ' campo reuquerido!', action: 'keyup, blur, change', rule: 'required' },
		{input: '#txtMail', message: ' debe ingresar un correo valido!', action: 'keyup, blur, change', rule: 'email' },
	   	{input: '#txtAddress', message: ' campo reuquerido!', action: 'keyup, blur', rule: 'required' },
	   	{input: '#txtPhone', message: ' campo reuquerido!', action: 'keyup, blur', rule: 'required' },
	   	{input: '#txtStartDate', message: ' campo reuquerido!', action: 'keyup, blur', rule: 'required' }
	   
	  ]
	 });
	$('#userForm').on('validationError', function (event)
	{
		console.log("falla");
		console.debug(event);
		setTimeout(function(){ top(); }, 1000);
		
	});
	$('#userForm').on('validationSuccess', function (event)
	{
 		$('#formUser').jqxWindow('close');
	var url="saveuser";
	var roles = [];
	$("#cboRol").jqxComboBox('getSelectedItems').forEach(rol =>{
		roles.push(rol.value);
	});
	if($("#action").val() != "1")
	{
		url="updateuser";
	}	
	$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			cache: false,
			data: { "_token": $('meta[name="csrf-token"]').attr('content'), userID: $("#userid").val(), name: $("#txtName").val(), username: $("#txtUser").val(), password: $("#txtPassword").val(), email: $("#txtMail").val(), address: $("#txtAddress").val(), phone: $("#txtPhone").val(), position: $("#txtPosition").val(),  startDate: $("#txtStartDate").val(), status: $("#cboStatus").val(), picture: $("#showlogo").attr('src'), cargo: $("#cboPosition").val(), note: $("#txtNote").val(), rol: roles	},
			success: function(datos)
			{
				//$('#jqxLoader').jqxLoader('close');
				if(datos)
				{
					if(datos.result == "ok")
					{
						ShowNotificacionOk(datos.response, "containtmsg");
						getUsers();
					}
					else
					{
						ShowNotificacionError(datos.response, "containtmsg");
					}
				}				
			},
			error: function(datos)
			{
				$('#jqxLoader').jqxLoader('close');
				ShowNotificacionError('Error de Red!', "containtmsg");
			}
		});
 	});
 	function getUserRoles(userID){
 	$.ajax({
		type: "POST",
		dataType: "json",
		url: "/getuserrol",
		cache: false,
		data: { "_token": $('meta[name="csrf-token"]').attr('content'), userID: userID},
		success: function(datos)
		{
			//$('#jqxLoader').jqxLoader('close');
			if(datos)
			{
				
					datos.forEach( rol =>{
						$("#cboRol").jqxComboBox("selectItem", rol.id_rol);
					});
			}
				
		},
		error: function(datos)
		{
			$('#jqxLoader').jqxLoader('close');
			ShowNotificacionError('Error de Red!', "containtmsg");
		}
	});
 }
$('#usersGrid').on('rowdoubleclick', function (event) 
	{
		$("#action").val(2);
		$("#cboRol").jqxComboBox('clearSelection'); 
		$("#userid").val($('#usersGrid').jqxGrid('getrowdata', event.args.rowindex).id);
		getUserRoles($('#usersGrid').jqxGrid('getrowdata', event.args.rowindex).id);
		$("#txtName").val($('#usersGrid').jqxGrid('getrowdata', event.args.rowindex).name);
		$("#txtUser").val($('#usersGrid').jqxGrid('getrowdata', event.args.rowindex).username);
		$("#txtMail").val($('#usersGrid').jqxGrid('getrowdata', event.args.rowindex).email);
		$("#txtAddress").val($('#usersGrid').jqxGrid('getrowdata', event.args.rowindex).direccion);
		$("#cboPosition").val($('#usersGrid').jqxGrid('getrowdata', event.args.rowindex).cargo);
		$("#cboStatus").val(($('#usersGrid').jqxGrid('getrowdata', event.args.rowindex).status)?1:0);
		$("#txtStartDate").val($('#usersGrid').jqxGrid('getrowdata', event.args.rowindex).fecha_ingreso);
		$("#txtPhone").val($('#usersGrid').jqxGrid('getrowdata', event.args.rowindex).telefono);
		$("#txtNote").val($('#usersGrid').jqxGrid('getrowdata', event.args.rowindex).nota_baja);
		$("#showlogo").attr('src', $('#usersGrid').jqxGrid('getrowdata', event.args.rowindex).url_img);
		$('#formUser').jqxWindow('open'); 
	});
function clearform(){
	$("#noteContainer").addClass("hidden");
	$("#txtName").val("");
	$("#txtUser").val("");
	$("#txtMail").val("");
	$("#txtAddress").val("");
	$("#cboPosition").jqxComboBox('clearSelection'); 
	$("#txtStartDate").val("");
	$("#txtPhone").val("");
	$("#txtPassword").val("");
	$("#cboStatus").jqxComboBox('selectedIndex', 0); 
	$("#cboRol").jqxComboBox('clearSelection'); 
	$("#showlogo").attr('src', '');
}
$("#btnNew").on("click", function(){
	clearform();
	$("#action").val(1);
	$('#formUser').jqxWindow('open');
});
$("#btnSave").on("click", function(){
	$("#userForm").jqxValidator('validate');
});
$("#picture").on('change', function()
{	
	LeerURLImage(this);
});
function LeerURLImage(input)
{	
	if (input.files[0].size >= 5250000)
	{
		ShowNotificacionError('El archivo no puede ser mayor a 1 MB', "transfermsg");
		$("#picture").val("");
		$("#showlogo").attr('src', '');
		return;
	}		
	if (input.files[0].type == 'image/png' || input.files[0].type == 'image/jpeg' || input.files[0].type == 'image/gif')
	{
		var reader = new FileReader();
		reader.onload = function(e)
		{
			$("#showlogo").attr('src', e.target.result).width(input.files[0].width);
			$("#showlogo").attr('src', e.target.result).height(input.files[0].height);				
			imageObj = new Image();
			imageObj.src = e.target.result;
		}
		reader.readAsDataURL(input.files[0]);
	}
	/*else if (input.files[0].type == 'application/x-zip-compressed' || input.files[0].type == 'application/zip')
	{
		return;
	}*/
	else
	{
		ShowNotificacionError('Solo se permiten subir archivos tipo PNG, JPG, JPEG Y GIF', "transfermsg");
		$("#picture").val("");
		$("#showlogo").attr('src', '');
		return;
	}	
}
function ShowNotificacionError(texto, modal)
	{
		$("#wrongNotification").jqxNotification({ appendContainer: "#"+modal });
		$("#wrongNotification label").html(texto);
		$("#wrongNotification").jqxNotification("open");
	}

	function ShowNotificacionOk(texto)
	{
		$("#correctNotification label").html(texto);
		$("#correctNotification").jqxNotification("open");
	}
});//end ready event