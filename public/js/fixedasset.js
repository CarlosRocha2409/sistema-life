
$(document).ready(function() 
{	
	getCategories();
	getMeassures();
	getMangas();
	getMarcas();
	getColores();
	getDimensiones();
	getTipos();
	getTallas();
	getFabrics();
	getGeneros();
	getProducts();
	var down=null,assigned=null;
	var categories = [];
	var multiUrl="";
	$("#action").val(1);
	$("#correctNotification").jqxNotification
	({ 
		autoOpen: false, 
		closeOnClick: true, 
		autoClose: true, 
		template: "success",
		theme: "ui-start",
		appendContainer: "#containtmsg",
		width: "100%"
	});
	
    $("#wrongNotification").jqxNotification
	({ 
		autoOpen: false, 
		closeOnClick: true, 
		autoClose: true, 
		template: "error",
		theme: "ui-start",
		appendContainer: "#containtmsg",
		width: "100%"
	});
	if($("#btnfindasset").length >0)
	{
		$("#btnfind").jqxButton({ width: 120, height: 30, theme: 'ui-start'});
		$("#btnfindasset").jqxButton({ width: '105', height: '30', theme: 'ui-start'});   
	}
	if($("#saveasset").length > 0)
		$("#saveasset").jqxButton({ width: '105', height: '30', theme: 'ui-start'});
	if($("#btnassingasset").length > 0)
	{
		$("#btnassingasset").jqxButton({ width: '105', height: '30', theme: 'ui-start'});
		$("#asignar").jqxButton({ width: 120, height: 30, theme: 'ui-start'});
	}
	if($("#btndismissasset").length > 0)
		$("#btndismissasset").jqxButton({ width: '105', height: '30', theme: 'ui-start'});
	
	if($("#deprecate").length > 0)
		$("#deprecate").jqxButton({ width: '105', height: '30', theme: 'ui-start'});
	
	if($("#btnunassign").length > 0)
		$("#btnunassign").jqxButton({ width: '105', height: '30', theme: 'ui-start'});
	if($("#btnnewasset").length > 0)
		$("#btnnewasset").jqxButton({ width: '105', height: '30', theme: 'ui-start'});
	
	//$("#run").jqxButton({ width: '120', height: '30', theme: 'ui-start'});
	$("#txtCatName").jqxInput({height: 20, width: "80%", theme: 'ui-start', placeHolder: "Nombre de la Categoria" });
	$("#txtMulti").jqxInput({height: 20, width: "100%", theme: 'ui-start', placeHolder: "" });
	
	$("#txtProviderCode").jqxInput({height: 25, width: 269, theme: 'ui-start', placeHolder: "Ingrese el codigo de producto" });
	$("#txtProductCode").jqxInput({height: 25, width: 269, theme: 'ui-start', placeHolder: "Ingrese el codigo de producto" });
	$("#txtProductName").jqxInput({height: 25, width: 269, theme: 'ui-start', placeHolder: "Ingrese el nombre del producto" });
	$("#txtSerie").jqxInput({height: 25, width: 269, theme: 'ui-start', placeHolder: "Ingrese Numero de Serie" });
	//$("#initial_value").jqxNumberInput({spinButtons: true, spinButtonsStep: 1,  decimalDigits: 2, height: 25, width: 275, theme: 'ui-start', digits:15, max:999999999999999 }); 
	$("#txtOptionalCode1").jqxInput({height: 25, width: 269, theme: 'ui-start', placeHolder: "Codigo opcional 1" });
	$("#txtOptionalCode2").jqxInput({height: 25, width: 269, theme: 'ui-start', placeHolder: "Codigo opcional 2" });
	$('#txtDescription').jqxTextArea({ placeHolder: "Ingrese descripcion del productos", height: 80, width: 275, minLength: 1, theme: 'ui-start' });
	//$("#buy_date").jqxDateTimeInput({ width: 275, height: 30,  formatString: 'F', theme: 'ui-start', value: new Date() });
	//$("#btndowndeprecation").jqxButton({ width: '120', height: '30', theme: 'ui-start'});
	//$("#btndowndeprecation").hide();
	if($("#download").length > 0)
		$("#download").jqxButton({ width: '120', height: '30', theme: 'ui-start'});

	//$("#bill_number").jqxInput({height: 25, width: 269, theme: 'ui-start', placeHolder: "Input Bill Number" });
	//$("#location").jqxInput({height: 25, width: 269, theme: 'ui-start', placeHolder: "Input Asset Location" });
	//$('#photo_asset').filestyle();
	//$("#deprecable").jqxSwitchButton({ theme: 'ui-start', width: 70, height: 25,  onLabel:'Yes',  offLabel:'No', checked:false, thumbSize:'50%' });         
//	$("#dateinitial").jqxDateTimeInput({ width: 270, height: 25,  formatString: 'F', theme: 'ui-start' });
//	$("#dateend").jqxDateTimeInput({ width: 270, height: 25,  formatString: 'F', theme: 'ui-start' });
	$("#jqxLoader").jqxLoader({ isModal: true, width: 100, height: 60, imagePosition: 'center', theme: 'ui-start' });
	$("#jqxLoader").css('z-index', 99999);
	$("#cboTypeInventory").jqxComboBox({ source: categories, width: 275, height: 25, theme: "ui-start", promptText: "Seleccione la categoria", displayMember: 'nombre', valueMember: 'id_category'});  
	$("#cboBrand").jqxComboBox({ source: [], width: 275, height: 25, theme: "ui-start", promptText: "Seleccione Marca", displayMember: 'marca', valueMember: 'id_marca'});
	$("#cboStatus").jqxComboBox({ source: [{id_status:0, status: "Inactivo"}, {id_status:1, status:"Activo"}], width: 275, height: 25, theme: "ui-start", promptText: "Seleccione Estado del producto", displayMember: 'status', valueMember: 'id_status', selectedIndex:1});
	$("#cboGender").jqxComboBox({ source: [], width: 275, height: 25, theme: "ui-start", promptText: "Seleccione Genero", displayMember: 'genero', valueMember: 'id_genero'});
	$("#cboColor").jqxComboBox({ source: [], width: 275, height: 25, theme: "ui-start", promptText: "Seleccione Color", displayMember: 'color', valueMember: 'id_color'});
	$("#cboSize").jqxComboBox({ source: [], width: 275, height: 25, theme: "ui-start", promptText: "Seleccione Talla", displayMember: 'talla', valueMember: 'id_talla'});
	$("#cboMeasureUnit").jqxComboBox({ source: [], width: 275, height: 25, theme: "ui-start", promptText: "Seleccione Unidad de Medida", displayMember: 'unidad_medida', valueMember: 'id_unidad_medida'});
	$("#cboTipo").jqxComboBox({ source: [], width: 275, height: 25, theme: "ui-start", promptText: "Seleccione Tipo", displayMember: 'tipo', valueMember: 'id_tipo'});
	$("#cboTela").jqxComboBox({ source: [], width: 275, height: 25, theme: "ui-start", promptText: "Seleccione Tela", displayMember: 'tela', valueMember: 'id_tela'});
	$("#cboTManga").jqxComboBox({ source: [], width: 275, height: 25, theme: "ui-start", promptText: "Seleccione el Tipo de Manga", displayMember: 'tipo_manga', valueMember: 'id_tipo_manga'});
	$("#cboDimension").jqxComboBox({ source: [], width: 275, height: 25, theme: "ui-start", promptText: "Seleccione Dimension", displayMember: 'dimension', valueMember: 'id_dimension'});
	//brands();
	//assetstatus();
	//employees();
	//getmonths();
	//getYears();
	//assetClassification();
	//assetClass();
	$("#photo_asset").change(function(){
	    readURL(this);
	});

	$('#formCategory').jqxWindow({
        autoOpen: false,
        resizable: true,
        width: (window.innerWidth*0.5),
        maxWidth:window.innerWidth,
        isModal: true,
        height: (window.innerHeight*0.6),
        maxHeight:window.innerHeight,
        theme: 'ui-start',
        draggable: true,
        initContent: function ()
        {
			//$('#listboxticketsc').jqxWindow('focus');
		}
	 });
		$('#multiForm').jqxWindow({
        autoOpen: false,
        resizable: true,
        width: (window.innerWidth*0.5),
        maxWidth:window.innerWidth,
        isModal: true,
        height: (window.innerHeight*0.4),
        maxHeight:window.innerHeight,
        theme: 'ui-start',
        draggable: true,
        initContent: function ()
        {
			//$('#listboxticketsc').jqxWindow('focus');
		}
	 });

	$("#btnNewCategory").on("click", function(){
		clearCategoryModal();
		$('#formCategory').jqxWindow('open');
	});
	$("#btnNewMarca").on("click", function(){
		$("#multiModalTitle").html(" Crear Nueva Marca");
		$("#lblMulti").html(" Marca:");
		$("#lblMulti").jqxInput("placeholder", "Ingrese Marca");
		multiUrl = "/saveMarca";
		$('#multiForm').jqxWindow('open');
	});
	$("#btnNewMeasure").on("click", function(){
		$("#multiModalTitle").html(" Crear Nueva Unidad de Medida");
		$("#lblMulti").html(" Unidad de Medida:");
		$("#lblMulti").jqxInput("placeholder", "Ingrese Unidad de Medida");
		multiUrl = "/saveMeasure";
		$('#multiForm').jqxWindow('open');
	});
	$("#btnNewGender").on("click", function(){
		$("#multiModalTitle").html(" Crear Nuevo Genero");
		$("#lblMulti").html(" Genero:");
		$("#lblMulti").jqxInput("placeholder", "Ingrese Genero");
		multiUrl = "/saveGenero";
		$('#multiForm').jqxWindow('open');
	});
	$("#btnNewSize").on("click", function(){
		$("#multiModalTitle").html(" Crear Nueva Talla");
		$("#lblMulti").html(" Talla:");
		$("#lblMulti").jqxInput("placeholder", "Ingrese Talla");
		multiUrl = "/saveTalla";
		$('#multiForm').jqxWindow('open');
	});
	$("#btnNewColor").on("click", function(){
		$("#multiModalTitle").html(" Crear Nuevo Color");
		$("#lblMulti").html(" Color:");
		$("#lblMulti").jqxInput("placeholder", "Ingrese Color");
		multiUrl = "/saveColor";
		$('#multiForm').jqxWindow('open');
	});
	$("#btnNewDimension").on("click", function(){
		$("#multiModalTitle").html(" Crear Nueva Dimension");
		$("#lblMulti").html(" Dimension:");
		$("#lblMulti").jqxInput("placeholder", "Ingrese Dimension:");
		multiUrl = "/saveDimension";
		$('#multiForm').jqxWindow('open');
	});
	$("#btnNewTManga").on("click", function(){
		$("#multiModalTitle").html(" Crear Nuevo Tipo de Manga");
		$("#lblMulti").html(" Tipo de Manga:");
		$("#lblMulti").jqxInput("placeholder", "Ingrese Tipo de Manga");
		multiUrl = "/saveManga";
		$('#multiForm').jqxWindow('open');
	});
	$("#btnNewTela").on("click", function(){
		$("#multiModalTitle").html(" Crear Nueva tela");
		$("#lblMulti").html(" Tela:");
		$("#lblMulti").jqxInput("placeholder", "Ingrese Tela");
		multiUrl = "/saveTela";
		$('#multiForm').jqxWindow('open');
	});
	$("#btnSaveMultiModal").click(function(){

		multiSave();
	});
	$("#btnFindCategory").click(function(){
		if($("#txtCatName").val()==''){
			ShowNotificacionError('el nombre de la categoria es un campo obligatorio', "messageModalCategories");
			return;
		}else{
			let data = categories.filter(cat => cat.nombre == $("#txtCatName").val());
			if(data.length > 0){
				$("#id_category_modal").val(data[0].id_category);
				$("#categoryTypeAction").val(2);
				data[0].marca==1? $("#chkMarca").prop("checked", true) : $("#chkMarca").prop("checked", false); 
				data[0].genero==1? $("#chkGenero").prop("checked", true) : $("#chkGenero").prop("checked", false); 
				data[0].color==1? $("#chkColor").prop("checked", true) : $("#chkColor").prop("checked", false); 
				data[0].talla==1? $("#chkTalla").prop("checked", true) : $("#chkTalla").prop("checked", false);
				data[0].tipo_manga==1? $("#chkTipoManga").prop("checked", true) : $("#chkTipoManga").prop("checked", false);
				data[0].dimensiones==1? $("#chkDimensiones").prop("checked", true) : $("#chkDimensiones").prop("checked", false);
				data[0].codigo_proveedor==1? $("#chkPCode").prop("checked", true) : $("#chkPCode").prop("checked", false);
				data[0].unidad_medida==1? $("#chkUMedida").prop("checked", true) : $("#chkUMedida").prop("checked", false);
				data[0].serie==1? $("#chkSerie").prop("checked", true) : $("#chkSerie").prop("checked", false);

			}else{
				ShowNotificacionError('No se econtó categoria con el nombre ingresado' , "messageModalCategories");
				clearCategoryModal();
			}
		}

	});
	
	$("#cboTypeInventory").on("select", function(e) 
	{
		prepareForm(parseInt($("#cboTypeInventory").val()));
	});
	$("#btnnewasset").click(function(){
		clearassetform();
	});
	function getCategories(){
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "/getPrdCategories",
			cache: false,
			success: function(datos)
			{
				categories = datos;
				$("#cboTypeInventory").jqxComboBox('source', categories);
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener las categorias', "containtmsg");
				top();
			}
		});
	}
	function prepareForm(id_category){
		var data = categories.filter(cat => cat.id_category  == id_category);
		if(data.length > 0){
			data[0].marca==1?$(".marca").removeClass("hidden"):$(".marca").addClass("hidden");
			data[0].genero==1?$(".genero").removeClass("hidden"):$(".genero").addClass("hidden");
			data[0].color==1?$(".color").removeClass("hidden"):$(".color").addClass("hidden");
			data[0].talla==1?$(".talla").removeClass("hidden"):$(".talla").addClass("hidden");
			//data[0].tipo==1?$(".tipo").removeClass("hidden");:$(".tipo").addClass("hidden");
			data[0].tela==1?$(".tela").removeClass("hidden"):$(".tela").addClass("hidden");
			data[0].tipo_manga==1?$(".tipo_manga").removeClass("hidden"):$(".tipo_manga").addClass("hidden");
			data[0].dimensiones==1?$(".dimensiones").removeClass("hidden"):$(".dimensiones").addClass("hidden");
			data[0].codigo_proveedor==1?$(".codigo_proveedor").removeClass("hidden"):$(".codigo_proveedor").addClass("hidden");
			data[0].unidad_medida==1?$(".unidad_medida").removeClass("hidden"):$(".unidad_medida").addClass("hidden");
			data[0].serie==1?$(".serie").removeClass("hidden"):$(".serie").addClass("hidden");

		}else{
			ShowNotificacionError('Se ha producido un error al obtener las categorias', "containtmsg");
		}
	}
	$("#btnSaveCategory").click(function(){
		if($("#txtCatName").val()==''){
			ShowNotificacionError('el nombre de la categoria es un campo obligatorio', "messageModalCategories");
			return;
	}
		saveCategory();
	});
	function saveCategory(){
		var _url = parseInt($("#categoryTypeAction").val())==1?"saveCategory":"updateCategory";
		$.ajax({
			type: "POST",
			dataType: "json",
			url: _url,
			cache: false,
			data: { id_category: $("#id_category_modal").val(), nombre: $("#txtCatName").val(), marca:($("#chkMarca").prop("checked")==true?1:0), 
				genero:($("#chkGenero").prop("checked")==true?1:0), color: ($("#chkColor").prop("checked")==true?1:0), talla: ($("#chkTalla").prop("checked")==true?1:0), 
				tipo_manga: ($("#chkTipoManga").prop("checked")==true?1:0), dimensiones: ($("#chkDimensiones").prop("checked")==true?1:0), codigo_proveedor: ($("#chkPCode").prop("checked")==true?1:0),
				unidad_medida: ($("#chkUMedida").prop("checked")==true?1:0), serie:($("#chkSerie").prop("checked")==true?1:0)
			},
			success: function(datos)
			{
				getCategories();				
				$("#formCategory").jqxWindow("close");
				clearCategoryModal();
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al guardar/actualizar las categorias', "messageModalCategories");
				top();
			}
		});
	}
	function getColores(){
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "/getColores",
			cache: false,
			success: function(datos)
			{
				$("#cboColor").jqxComboBox('source', datos);
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener los colores', "containtmsg");
				top();
			}
		});
	}
	function getDimensiones(){
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "/getDimensiones",
			cache: false,
			success: function(datos)
			{
				$("#cboDimension").jqxComboBox('source', datos);
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener las dimensiones', "containtmsg");
				top();
			}
		});
	}
	function getMeassures(){
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "/getMeasures",
			cache: false,
			success: function(datos)
			{
				$("#cboMeasureUnit").jqxComboBox('source', datos);
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener las unidades de medida', "containtmsg");
				top();
			}
		});
	}
	function getMarcas(){
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "/getMarcas",
			cache: false,
			success: function(datos)
			{
				$("#cboBrand").jqxComboBox('source', datos);
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener las Marcas', "containtmsg");
				top();
			}
		});
	}
	function getTallas(){
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "/getTallas",
			cache: false,
			success: function(datos)
			{
				$("#cboSize").jqxComboBox('source', datos);
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener las tallas', "containtmsg");
				top();
			}
		});
	}
	function getTipos(){
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "/getTipos",
			cache: false,
			success: function(datos)
			{
				$("#cboTipo").jqxComboBox('source', datos);
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener los tipos', "containtmsg");
				top();
			}
		});
	}
	function getFabrics(){
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "/getTelas",
			cache: false,
			success: function(datos)
			{
				$("#cboTela").jqxComboBox('source', datos);
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener las telas', "containtmsg");
				top();
			}
		});
	}
	function getMangas(){
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "/getMangas",
			cache: false,
			success: function(datos)
			{
				$("#cboTManga").jqxComboBox('source', datos);
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener los tipos de mangas', "containtmsg");
				top();
			}
		});
	}
	function getGeneros(){
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "/getGeneros",
			cache: false,
			success: function(datos)
			{
				$("#cboGender").jqxComboBox('source', datos);
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener los tipos de mangas', "containtmsg");
				top();
			}
		});
	}
	function multiSave(){
		$.ajax({
			type: "POST",
			dataType: "json",
			url: multiUrl,
			cache: false,
			data: { value: $("#txtMulti").val()},
			success: function(datos)
			{
				$('#txtMulti').val('');
				getMeassures();
				getMangas();
				getMarcas();
				getColores();
				getDimensiones();
				getTipos();
				getTallas();
				getFabrics();
				getGeneros();
				$("#multiForm").jqxWindow("close");
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener al crear el registro', "containtmsg");
				top();
			}
		});
	}
$('#inventoryForm').jqxValidator(
	 {
	  theme: 'ui-start',
	  animation: 'fade',
	  position: 'topcenter',
	  animationDuration: 1000,
	  rules: 
	  [
	    {input: '#cboTypeInventory', message: 'Campo Requerido!', action: 'select, keyup, blur', rule:  
	   		function (input, commit) 
	   		{ 
	     		if($("#cboTypeInventory").jqxComboBox('getSelectedIndex') == -1)
		     	{ 
		     		return false; 
		     	}
		     	else 
		     	{
		     		return true; 
		     	}
	   		}
	    },
	   	{input: '#txtProductCode', message: ' Campo Requerido!', action: 'keyup, blur', rule: 'required' },
	   	{input: '#txtProductName', message: ' Campo Requerido!', action: 'keyup, blur', rule: 'required' },
	   	{input: '#txtDescription', message: ' Campo Requerido!', action: 'keyup, blur', rule: 'required' },
	  ]
	 });
	$('#inventoryForm').on('validationError', function (event)
	{
		setTimeout(function(){ top(); }, 1000);
		
	});
	$('#inventoryForm').on('validationSuccess', function (event)
	{
		$.confirm({
			icon: 'fa fa-floppy-o',
			title: 'Confirmar',
			content: 'Quieres guardar/actualizar este producto?',
			type: 'blue',
			typeAnimated: true,
		    closeIcon: true,
		    closeIconClass: 'fa fa-close',
		    draggable: true,
		    animation: 'zoom',
			closeAnimation: 'scale',
			animationBounce: 2,
			animationSpeed: 300,
			buttons:{
					Si:{
							btnClass: 'btn-danger',
							keys: ['enter', 'y','Y'],
							action: function()
							{
								if(parseInt($("#action").val()) == 1){
									save();
								}
								else if(parseInt($("#action").val()) == 2){
									update();
								}
								this.close();
							}
						},
					No:{
							btnClass: 'btn-info',
							keys: ['esc','n','N'],
							action: function()
							{
								return;
							}
						}
			
					},
			
		});
							
						
 	});
		$("#cbomonths").change(function(){
		if($("#cbomonths").val())
		{
			$("#btndowndeprecation").show(1000);
		}
		else
		{
			$("#btndowndeprecation").hide(1000);
		}
	});
	$("#download").click(function(){
		window.open('downloadassetList');
	});
	$("#btndowndeprecation").click(function(){
		var month= $("#cbomonths").val();
		var year= $("#cboyears").val();
		$("#cbomonths").jqxComboBox("clearSelection");
		window.open('downloaddepreciationdetail?month='+ month+'&year='+year);
	});
	function depreciationProcess()
	{
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "deprecate",
			cache: false,
			data: { month: $("#cbomonths").val(), year: $("#cboyears").val()},
			success: function(datos)
			{
				if(datos)
				{
					$(".lock").prop('disabled',false);
					if(datos.result == "ok")
					{
						$("#action").val(1);
						$('#jqxLoader').jqxLoader('close');
						ShowNotificacionOk(datos.response , "containtmsg");
						clearassetform();
						$("#btndowndeprecation").show(1000);
					}
					else
					{
						//ClearTicketForm();
						$('#formdepreciate').jqxWindow('close');
						$('#jqxLoader').jqxLoader('close');
						ShowNotificacionError(datos.response, "containtmsg");
						top();
					}
				}				
			},
			error: function(datos)
			{
				$(".lock").prop('disabled',false);
				$('#jqxLoader').jqxLoader('close');
				$('#formdepreciate').jqxWindow('close');
				ShowNotificacionError('Se ha producido un error al ejecutar el proceso de depreciation', "containtmsg");
				top();
			}
		});
	}
	$("#btnSave").click(function(){

		$('#inventoryForm').jqxValidator('validate');
		
		
	});
	$("#btnFind").click(function(){
		$("#searchSection").removeClass("hidden");
		$("#formSection").addClass("hidden");
		clearProductForm();
	});
	$("#btnNew").click(function(){
		$("#searchSection").addClass("hidden");
		$("#formSection").removeClass("hidden");
		clearProductForm();
	});
	function save(){
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "/saveProduct",
			cache: false,
			data: { product_code: $("#txtProductCode").val(), optional_code1: $("#txtOptionalCode1").val(), optional_code2: $("#txtOptionalCode2").val(), product_name: $("#txtProductName").val(), id_category:$("#cboTypeInventory").val(), id_status:$("#cboStatus").val(), product_description: $("#txtDescription").val(), image: $("#preview").attr('src'), id_marca:$("#cboBrand").val(), id_dimension:$("#cboDimension").val(), id_genero:$("#cboGender").val(), id_talla:$("#cboSize").val(), id_tela:$("#cboTela").val(), id_color:$("#cboColor").val(), id_tipo_manga:$("#cboTManga").val(), provider_code: $("#txtProviderCode").val(), id_unidad_medida:$("#cboMeasureUnit").val(), numero_serie:$("#txtSerie").val()},
			success: function(datos)
			{
				if(datos)
				{
					if(datos.resultado == "OK")
					{
						$("#action").val(1);
						getProducts();
						$("#btnFind").trigger("click");
					}
					else
					{
						ShowNotificacionError("Error de Insercion", "containtmsg");
						top();
					}
				}				
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al insertar el producto', "containtmsg");
				top();
			}
		});
	}
   	function update(){
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "/updateProduct",
			cache: false,
			data: { id_product: $("#idProduct").val(), product_code: $("#txtProductCode").val(), optional_code1: $("#txtOptionalCode1").val(), optional_code2: $("#txtOptionalCode2").val(), product_name: $("#txtProductName").val(), id_category:$("#cboTypeInventory").val(), id_status:$("#cboStatus").val(), product_description: $("#txtDescription").val(), image: $("#preview").attr('src'), id_marca:$("#cboBrand").val(), id_dimension:$("#cboDimension").val(), id_genero:$("#cboGender").val(), id_talla:$("#cboSize").val(), id_tela:$("#cboTela").val(), id_color:$("#cboColor").val(), id_tipo_manga:$("#cboTManga").val(), provider_code: $("#txtProviderCode").val(), id_unidad_medida:$("#cboMeasureUnit").val(), numero_serie:$("#txtSerie").val()},
			success: function(datos)
			{
				if(datos)
				{
					if(datos.resultado == "OK")
					{
						$("#action").val(1);
						getProducts();
						$("#btnFind").trigger("click");
					}
					else
					{
						ShowNotificacionError("Error de Insercion", "containtmsg");
						top();
					}
				}				
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al insertar el producto', "containtmsg");
				top();
			}
		});
	}
	$("#btnDownload").click(function(){
		window.open("/downloadProducts","_blank", "width=10 height=10");
	});
	
	
	function getProducts()
	{

		var source =
        {
			type: "GET",
			datatype: "json",
			
            datafields: [
				{ name: 'id_product', type: 'integer'},
				{ name: 'product_code', type: 'string'},
				{ name: 'optional_code1', type: 'string'},
				{ name: 'optional_code2', type: 'string'},
				{ name: 'product_name', type: 'string'},
				{ name: 'id_category', type: 'integer'},
				{ name: 'category', type: 'string'},
				{ name: 'id_marca', type: 'integer'},
				{ name: 'marca', type: 'string'},
				{ name: 'id_status', type: 'integer'},
				{ name: 'status', type: 'string'},
				{ name: 'product_description', type: 'string'},
				{ name: 'image', type: 'string'},
				{ name: 'id_dimension', type: 'integer'},
				{ name: 'dimension', type: 'string'},
				{ name: 'id_genero', type: 'integer'},
				{ name: 'genero', type: 'string'},
				{ name: 'id_talla', type: 'integer'},
				{ name: 'talla', type: 'string'},
				{ name: 'id_tela', type: 'integer'},
				{ name: 'tela', type: 'string'},
				{ name: 'id_color', type: 'integer'},
				{ name: 'color', type: 'string'},
				{ name: 'id_tipo_manga', type: 'integer'},
				{ name: 'tipo_manga', type: 'string'},
				{ name: 'provider_code', type: 'string'},
				{ name: 'id_unidad_medida', type: 'integer'},
				{ name: 'unidad_medida', type: 'string'},
				{ name: 'numero_serie', type: 'string'},
				{ name: 'id_user_created', type: 'integer'},
				{ name: 'user_created', type: 'string'},
				{ name: 'date_created', type: 'string'},

			],
            url: '/getProducts',
			cache: false
        };
		
		var productsAdapter = new $.jqx.dataAdapter(source);
		
		$("#gridProducts").jqxGrid(
        {
			width: '100%',
			source: productsAdapter,
			theme: 'ui-start',
			width: "100%",
			autoheight: "true",
			columnsresize: true,
			selectionmode: 'singlerow',
			pageable: true,
			enabletooltips: true,
			sortable: true,
			editable: false,
			showfilterrow: true,
        	filterable: true,
			enablebrowserselection: true,
			pagermode: "simple",
			pagesize: 20,
		 	altrows: true,
			columns: [
	
                  { text: 'Codigo', datafield: 'id_product', displayfield: 'product_code', width: 90},
				  { text: 'Nombre', datafield: 'product_name', displayfield: 'product_name', width: 130},
				  { text: 'Descripcion', datafield: 'product_description', displayfield: 'product_description', width: 200},
				  { text: 'Categoria', datafield: 'id_category', displayfield: 'category', width: 90},
				  { text: 'Marca', datafield: 'id_marca', displayfield: 'marca', width: 100},
				  { text: 'Dimensiones', datafield: 'id_dimension', displayfield: 'dimension', width: 90},
				  { text: 'Codigo Opcional 1', datafield: 'optional_code1', displayfield: 'optional_code1', width: 100},
				  { text: 'Codigo Opcional 2', datafield: 'optional_code2', displayfield: 'optional_code2', width: 100},
				  { text: 'Codigo Proveedor', datafield: 'provider_code', displayfield: 'provider_code', width: 200},
				  { text: 'Genero', datafield: 'id_genero', displayfield: 'genero'},
				  { text: 'Talla', datafield: 'id_talla', displayfield: 'talla'},
				  { text: 'Tela', datafield: 'id_tela', displayfield: 'tela'},
				  { text: 'Color', datafield: 'id_color', displayfield: 'color'},
  				  { text: 'Estado', datafield: 'id_status', displayfield: 'status'},

			]
		});
	}
	
	
	
	$('#gridProducts').on('rowdoubleclick', function (event) 
	{
		clearProductForm();
		$("#action").val(2);
		$("#cboTypeInventory").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).id_category);
		$("#cboTypeInventory").trigger('select');
		$("#idProduct").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).id_product);
		$("#txtProductCode").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).product_code);
		$("#txtOptionalCode1").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).optional_code1);
		$("#txtOptionalCode2").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).optional_code2);
		$("#txtProductName").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).product_name);
		$("#txtDescription").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).product_description);
		$("#cboBrand").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).id_marca);
		$("#cboMeasureUnit").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).id_unidad_medida);
		$("#cboGender").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).id_genero);
		$("#cboSize").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).id_talla);
		$("#cboTela").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).id_tela);
		$("#cboColor").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).id_color);
		$("#cboTManga").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).id_tipo_manga);
		$("#cboDimension").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).id_dimension);
		$("#txtProviderCode").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).provider_code);
		$("#txtSerie").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).numero_serie);
		$("#preview").attr('src', $('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).image);
		$("#cboStatus").val($('#gridProducts').jqxGrid('getrowdata', event.args.rowindex).id_status);
		$("#searchSection").addClass("hidden");
		$("#formSection").removeClass("hidden");
	});
	
	function saveasset()
	{
		var picture =  $("#preview").attr('src');
		var deprecated = $("#deprecable").jqxSwitchButton('checked') == true? 1:0; 
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "saveasset",
			
			data: {  id_asset_class: $("#cboaclass").val(), id_asset_classif: $("#cboaclassif").val(), id_category: $("#cbocategories").val(), id_brand: $("#cbobrands").val(), id_status: $("#cboassetstatus").val(), code_asset: $("#code_asset").val(), name_asset: $("#name_asset").val(), initial_value: $("#initial_value").val(), 
			solved_value: $("#solved_value").val(), actual_value: $("#actual_value").val(), code_optional1: $("#code_optional1").val(), code_optional2: $("#code_optional2").val(), description: $("#description").val(), buy_date: $("#buy_date").val(), bill_number: $("#bill_number").val(), location: $("#location").val(), photo_asset: picture, deprecable: deprecated},
			success: function(datos) 
			{
				if(datos)
				{
					$(".lock").prop('disabled',false);
					if(datos.result == "ok")
					{
						
						down=0;
						$("#idasset").val(datos.response);
						$("#action").val(2);
						$('#jqxLoader').jqxLoader('close');
						ShowNotificacionOk('El Activo ' + datos.response + ' ha sido Creado exitosamente');
						top();
					}
					else
					{
						//ClearTicketForm();
						$('#jqxLoader').jqxLoader('close');
						ShowNotificacionError('Se ha producido error favor valide e intente nuevamente', "containtmsg");
						top();
					}
				}				
			},
			error: function(datos)
			{
				$(".lock").prop('disabled',false);
				$('#jqxLoader').jqxLoader('close');
				ShowNotificacionError('Se ha producido un error al guardar el activo', "containtmsg");
				top();
			}
		});
	}

	function updateasset()
	{
		var picture =  $("#preview").attr('src');
		var deprecated = $("#deprecable").jqxSwitchButton('checked') == true? 1:0; 
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "updateasset",
			
			data: {  id_asset_class: $("#cboaclass").val(), id_asset_classif: $("#cboaclassif").val(), id_asset: $("#idasset").val(), id_category: $("#cbocategories").val(), id_brand: $("#cbobrands").val(), id_status: $("#cboassetstatus").val(), code_asset: $("#code_asset").val(), name_asset: $("#name_asset").val(), initial_value: $("#initial_value").val(), 
			solved_value: $("#solved_value").val(), actual_value: $("#actual_value").val(), code_optional1: $("#code_optional1").val(), code_optional2: $("#code_optional2").val(), description: $("#description").val(), buy_date: $("#buy_date").val(), bill_number: $("#bill_number").val(), location: $("#location").val(), photo_asset: picture, deprecable: deprecated},
			success: function(datos) 
			{
				if(datos)
				{
					$(".lock").prop('disabled',false);
					if(datos.result == "ok")
					{
						$("#idasset").val(datos.response);
						$("#action").val(2);
						$('#jqxLoader').jqxLoader('close');
						ShowNotificacionOk('El Activo ' + datos.response + ' se ha actualizado exitosamente');
						top();
					}
					else
					{
						//ClearTicketForm();
						$('#jqxLoader').jqxLoader('close');
						ShowNotificacionError('Se ha producido error favor valide e intente nuevamente', "containtmsg");
						top();
					}
				}				
			},
			error: function(datos)
			{
				$(".lock").prop('disabled',false);
				$('#jqxLoader').jqxLoader('close');
				ShowNotificacionError('Se ha producido un error al asignar el activo', "containtmsg");
				top();
			}
		});
	}

	function assignasset()
	{
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "assignasset",
			cache: false,
			data: { asset_id: $("#idasset").val(), employee_id: $("#cboemployees").val() },
			success: function(datos)
			{
				if(datos)
				{
					$(".lock").prop('disabled',false);
					if(datos.result == "ok")
					{
						$('#formassign').jqxWindow('close');
						$("#idasset").val(datos.response);
						$("#action").val(2);
						$('#jqxLoader').jqxLoader('close');
						ShowNotificacionOk('El Activo ' + datos.response + ' se ha asignado con exito!', "containtmsg");
						top();
						getassethistory();
					}
					else
					{
						//ClearTicketForm();
						$('#formassign').jqxWindow('close');
						$('#jqxLoader').jqxLoader('close');
						ShowNotificacionError('Se ha producido error favor valide e intente nuevamente', "containtmsg");
						top();
					}
				}				
			},
			error: function(datos)
			{
				$(".lock").prop('disabled',false);
				$('#jqxLoader').jqxLoader('close');
				$('#formassign').jqxWindow('close');
				ShowNotificacionError('Se ha producido un error al guardar el activo', "containtmsg");
				top();
			}
		});
	}

	function dismissasset()
	{
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "dismissasset",
			cache: false,
			data: { asset_id: $("#idasset").val() },
			success: function(datos)
			{
				if(datos)
				{
					$(".lock").prop('disabled',false);
					if(datos.result == "ok")
					{
						$('#formassign').jqxWindow('close');
						$("#idasset").val(datos.response);
						$("#action").val(2);
						$('#jqxLoader').jqxLoader('close');
						ShowNotificacionOk('El Activo ' + datos.response + ' se ha dado de baja exitosamente!', "containtmsg");
						top();
						getassethistory();
						
					}
					else
					{
						//ClearTicketForm();
						$('#formassign').jqxWindow('close');
						$('#jqxLoader').jqxLoader('close');
						ShowNotificacionError('Se ha producido error favor valide e intente nuevamente', "containtmsg");
						top();
					}
				}				
			},
			error: function(datos)
			{
				$(".lock").prop('disabled',false);
				$('#jqxLoader').jqxLoader('close');
				$('#formassign').jqxWindow('close');
				ShowNotificacionError('Se ha producido un error al guardar el activo', "containtmsg");
				top();
			}
		});
	}

	function getassethistory()
	{
		$("#assetHidden").removeClass('hidden');
		var source2 =
        {
			type: "POST",
			datatype: "json",
			data: {asset_id: $("#idasset").val()},
            datafields: [
				{ name: 'id_history', type: 'integer'},
				{ name: 'id_asset', type: 'integer'},
				{ name: 'name_asset', type: 'string'},
				{ name: 'id_status', type: 'integer'},
				{ name: 'status', type: 'string'},
				{ name: 'id_personal', type: 'integer'},
				{ name: 'name_complete', type: 'string'},
				{ name: 'description', type: 'string'},
				{ name: 'id_user', type: 'integer'},
				{ name: 'user_name', type: 'string'},
				{ name: 'date_detail', type: 'date'},
			],
            url: 'assetHistory',
			cache: false
        };
		
		var HistoryAdapter = new $.jqx.dataAdapter(source2);
		
		$("#gridAssetHistory").jqxGrid(
        {
			width: '100%',
			source: HistoryAdapter,
			theme: 'ui-start',
			width: "100%",
			rowsheight: 25,
			autoheight: "true",
			columnsresize: true,
			selectionmode: 'singlerow',
			enabletooltips: true,
			sortable: true,
			enablebrowserselection: true,
			pageable: true,
			pagesize: 10,
			columns: [
	
                  { text: 'Asset Name', datafield: 'id_asset', displayfield: 'name_asset', width: 200},
				  { text: 'Asset Status', datafield: 'id_status', displayfield: 'status', width: 80},
				  { text: 'Personal Assigned', datafield: 'id_personal', displayfield: 'name_complete', width: 180},
				  { text: 'User', datafield: 'id_user', displayfield: 'user_name', width: 130},
				  { text: 'Description', datafield: 'description', displayfield: 'description', width: 280},
				  { text: 'Date', displayfield: 'date_detail', width: 200},
			]
		});
	}
	function readURL(input) 
	{

	    if (input.files && input.files[0]) 
	    {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#preview').attr('src', e.target.result);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	function top() 
	{
    	//document.body.scrollTop = 0; // For Chrome, Safari and Opera
    	//document.documentElement.scrollTop = 0; // For IE and Firefox
    	$("html, body").animate({ scrollTop: 0 }, 600);
	} 	

	function ShowNotificacionError(texto, modal)
	{
		$("#wrongNotification").jqxNotification({ appendContainer: "#"+modal });
		$("#wrongNotification label").html(texto);
		$("#wrongNotification").jqxNotification("open");
	}

	function ShowNotificacionOk(texto)
	{
		$("#correctNotification label").html(texto);
		$("#correctNotification").jqxNotification("open");
	}
	function clearCategoryModal(){
		$("#id_category_modal").val("");
		$("#categoryTypeAction").val(1);
		$("#txtCatName").val("");
		$("#chkMarca").prop("checked",false); 
		$("#chkGenero").prop("checked",false);
		$("#chkColor").prop("checked",false);
		$("#chkTalla").prop("checked",false); 
		$("#chkTipoManga").prop("checked",false);
		$("#chkDimensiones").prop("checked",false);
		$("#chkPCode").prop("checked",false);
		$("#chkUMedida").prop("checked",false);
		$("#chkSerie").prop("checked",false);

	}
	function clearProductForm()
	{
		$("#action").val(1);
		$("#idProduct").val('');
		$("#txtProductCode").val('');
		$("#txtOptionalCode1").val('');
		$("#txtOptionalCode2").val('');
		$("#txtProductName").val('');
		$("#cboTypeInventory").jqxComboBox('clearSelection');
		$("#cboStatus").jqxComboBox('clearSelection');
		$("#txtDescription").val('');
		$("#preview").attr('src', '');
		$("#cboBrand").jqxComboBox('clearSelection');
		$("#cboDimension").jqxComboBox('clearSelection');
		$("#cboGender").jqxComboBox('clearSelection');
		$("#cboSize").jqxComboBox('clearSelection');
		$("#cboTela").jqxComboBox('clearSelection');
		$("#cboColor").jqxComboBox('clearSelection');
		$("#cboTManga").jqxComboBox('clearSelection');
		$("#txtProviderCode").val('');
		$("#cboMeasureUnit").jqxComboBox('clearSelection');
		$("#txtSerie").val('');
		$(".serie, .codigo_proveedor, .marca, .dimensiones, .tipo_manga, .color, .tela, .tipo, .talla, .genero, .unidad_medida, .marca").addClass("hidden");
		setTimeout(function(){ top(); }, 700);

	}

});//end ready event