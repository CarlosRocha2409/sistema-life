
$(document).ready(function() 
{	
	getProviders();
	var down=null,assigned=null;
	var categories = [];
	var multiUrl="";
	$("#action").val(1);
	$("#correctNotification").jqxNotification
	({ 
		autoOpen: false, 
		closeOnClick: true, 
		autoClose: true, 
		template: "success",
		theme: "ui-start",
		appendContainer: "#containtmsg",
		width: "100%"
	});
	
    $("#wrongNotification").jqxNotification
	({ 
		autoOpen: false, 
		closeOnClick: true, 
		autoClose: true, 
		template: "error",
		theme: "ui-start",
		appendContainer: "#containtmsg",
		width: "100%"
	});
	if($("#btnFind").length >0)
	{
		$("#btnFind").jqxButton({ width: 120, height: 30, theme: 'ui-start'});
	}
	if($("#btnSave").length > 0)
		$("#btnSave").jqxButton({ width: '105', height: '30', theme: 'ui-start'});
	if($("#btnNew").length > 0)
	{
		$("#btnNew").jqxButton({ width: '105', height: '30', theme: 'ui-start'});
	}
	
	//$("#run").jqxButton({ width: '120', height: '30', theme: 'ui-start'});
	$("#txtName").jqxInput({height: 20, width: "80%", theme: 'ui-start', placeHolder: "Nombre del Proveedor" });
	$("#txtCommercialName").jqxInput({height: 20, width: "80%", theme: 'ui-start', placeHolder: "Nombre Comercial" });
	
	$("#txtCode").jqxInput({height: 20, width: "80%", theme: 'ui-start', placeHolder: "Ingrese el codigo del Proveedor" });
	$("#txtId").jqxInput({height: 20, width: "80%", theme: 'ui-start', placeHolder: "Ingrese el codigo de producto" });
	$("#txtCellphone").jqxInput({height: 20, width: "80%", theme: 'ui-start', placeHolder: "Ingrese Celular" });
	//$("#initial_value").jqxNumberInput({spinButtons: true, spinButtonsStep: 1,  decimalDigits: 2, height: 25, width: 275, theme: 'ui-start', digits:15, max:999999999999999 }); 
	$("#txtTelephone").jqxInput({height: 20, width: "80%", theme: 'ui-start', placeHolder: "Ingrese Telefono" });
	$("#txtEmail").jqxInput({height: 20, width: "80%", theme: 'ui-start', placeHolder: "Correo electronico" });
	$('#txtAddress').jqxTextArea({ placeHolder: "Ingrese Direccion", height: 80, width: "80%", minLength: 1, theme: 'ui-start' });
	//$("#buy_date").jqxDateTimeInput({ width: 275, height: 30,  formatString: 'F', theme: 'ui-start', value: new Date() });
	//$("#btndowndeprecation").jqxButton({ width: '120', height: '30', theme: 'ui-start'});
	//$("#btndowndeprecation").hide();
	//$("#bill_number").jqxInput({height: 25, width: 269, theme: 'ui-start', placeHolder: "Input Bill Number" });
	//$("#location").jqxInput({height: 25, width: 269, theme: 'ui-start', placeHolder: "Input Asset Location" });
	//$('#photo_asset').filestyle();
	//$("#deprecable").jqxSwitchButton({ theme: 'ui-start', width: 70, height: 25,  onLabel:'Yes',  offLabel:'No', checked:false, thumbSize:'50%' });         
//	$("#dateinitial").jqxDateTimeInput({ width: 270, height: 25,  formatString: 'F', theme: 'ui-start' });
//	$("#dateend").jqxDateTimeInput({ width: 270, height: 25,  formatString: 'F', theme: 'ui-start' });
	$("#jqxLoader").jqxLoader({ isModal: true, width: 100, height: 60, imagePosition: 'center', theme: 'ui-start' });
	$("#jqxLoader").css('z-index', 99999);
	$("#cboStatus").jqxComboBox({ source: [{id_status:0, status: "Inactivo"}, {id_status:1, status:"Activo"}], width: 275, height: 25, theme: "ui-start", promptText: "Seleccione Estado del producto", displayMember: 'status', valueMember: 'id_status', selectedIndex:1});
	//brands();
	//assetstatus();
	//employees();
	//getmonths();
	//getYears();
	//assetClassification();
	//assetClass();
	$("#photo_asset").change(function(){
	    readURL(this);
	});

	function getCategories(){
		$.ajax({
			type: "GET",
			dataType: "json",
			url: "/getPrdCategories",
			cache: false,
			success: function(datos)
			{
				categories = datos;
				$("#cboTypeInventory").jqxComboBox('source', categories);
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al obtener las categorias', "containtmsg");
				top();
			}
		});
	}
	
$('#providersForm').jqxValidator(
	 {
	  theme: 'ui-start',
	  animation: 'fade',
	  position: 'topcenter',
	  animationDuration: 1000,
	  rules: 
	  [
	    {input: '#cboStatus', message: 'Campo Requerido!', action: 'select, keyup, blur', rule:  
	   		function (input, commit) 
	   		{ 
	     		if($("#cboStatus").jqxComboBox('getSelectedIndex') == -1)
		     	{ 
		     		return false; 
		     	}
		     	else 
		     	{
		     		return true; 
		     	}
	   		}
	    },
	   	{input: '#txtName', message: ' Campo Requerido!', action: 'keyup, blur', rule: 'required' },
	   	{input: '#txtCommercialName', message: ' Campo Requerido!', action: 'keyup, blur', rule: 'required' },
	   	{input: '#txtCode', message: ' Campo Requerido!', action: 'keyup, blur', rule: 'required' },
	   	{input: '#txtId', message: ' Campo Requerido!', action: 'keyup, blur', rule: 'required' },
	   	{input: '#txtEmail', message: ' Campo Requerido!', action: 'keyup, blur', rule: 'required' },
	   	{input: '#txtTelephone', message: ' Campo Requerido!', action: 'keyup, blur', rule: 'required' },
	   	{input: '#txtCellphone', message: ' Campo Requerido!', action: 'keyup, blur', rule: 'required' },
	   	{input: '#txtAddress', message: ' Campo Requerido!', action: 'keyup, blur', rule: 'required' },
	  ]
	 });
	$('#providersForm').on('validationError', function (event)
	{
		setTimeout(function(){ top(); }, 1000);
		
	});
	$('#providersForm').on('validationSuccess', function (event)
	{
		$.confirm({
			icon: 'fa fa-floppy-o',
			title: 'Confirmar',
			content: 'Quieres guardar/actualizar este Proveedor?',
			type: 'blue',
			typeAnimated: true,
		    closeIcon: true,
		    closeIconClass: 'fa fa-close',
		    draggable: true,
		    animation: 'zoom',
			closeAnimation: 'scale',
			animationBounce: 2,
			animationSpeed: 300,
			buttons:{
					Si:{
							btnClass: 'btn-danger',
							keys: ['enter', 'y','Y'],
							action: function()
							{
								if(parseInt($("#action").val()) == 1){
									save();
								}
								else if(parseInt($("#action").val()) == 2){
									update();
								}
								this.close();
							}
						},
					No:{
							btnClass: 'btn-info',
							keys: ['esc','n','N'],
							action: function()
							{
								return;
							}
						}
			
					},
			
		});
							
						
 	});
		$("#cbomonths").change(function(){
		if($("#cbomonths").val())
		{
			$("#btndowndeprecation").show(1000);
		}
		else
		{
			$("#btndowndeprecation").hide(1000);
		}
	});
	$("#download").click(function(){
		window.open('downloadassetList');
	});
	$("#btndowndeprecation").click(function(){
		var month= $("#cbomonths").val();
		var year= $("#cboyears").val();
		$("#cbomonths").jqxComboBox("clearSelection");
		window.open('downloaddepreciationdetail?month='+ month+'&year='+year);
	});

	$("#btnSave").click(function(){

		$('#providersForm').jqxValidator('validate');
		
		
	});
	$("#btnFind").click(function(){
		$("#searchSection").removeClass("hidden");
		$("#formSection").addClass("hidden");
		clearProviderForm();
	});
	$("#btnNew").click(function(){
		$("#searchSection").addClass("hidden");
		$("#formSection").removeClass("hidden");
		clearProviderForm();
	});
	function save(){
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "/saveProvider",
			cache: false,
			data: { codigo_proveedor: $("#txtCode").val(), nombre: $("#txtName").val(), nombre_comercial: $("#txtCommercialName").val(), identificacion: $("#txtId").val(), correo:$("#txtEmail").val(), id_status:$("#cboStatus").val(), telefono: $("#txtTelephone").val(), celular: $("#txtCellphone").val(), direccion:$("#txtAddress").val()},
			success: function(datos)
			{
				if(datos)
				{
					if(datos.resultado == "OK")
					{
						$("#action").val(1);
						getProviders();
						$("#btnFind").trigger("click");
					}
					else
					{
						ShowNotificacionError("Error de Insercion", "containtmsg");
						top();
					}
				}				
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al insertar el Proveedor', "containtmsg");
				top();
			}
		});
	}
   	function update(){
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "/updateProvider",
			cache: false,
			data: { id_proveedor: $("#providerId").val(),  codigo_proveedor: $("#txtCode").val(), nombre: $("#txtName").val(), nombre_comercial: $("#txtCommercialName").val(), identificacion: $("#txtId").val(), correo:$("#txtEmail").val(), id_status:$("#cboStatus").val(), telefono: $("#txtTelephone").val(), celular: $("#txtCellphone").val(), direccion:$("#txtAddress").val()},
			success: function(datos)
			{
				if(datos)
				{
					if(datos.resultado == "OK")
					{
						$("#action").val(1);
						getProviders();
						$("#btnFind").trigger("click");
					}
					else
					{
						ShowNotificacionError("Error de Insercion", "containtmsg");
						top();
					}
				}				
			},
			error: function(datos)
			{
				ShowNotificacionError('Se ha producido un error al actualizar el Proveedor', "containtmsg");
				top();
			}
		});
	}
	$("#btnDownload").click(function(){
		window.open("/downloadProducts","_blank", "width=10 height=10");
	});
	
	
	function getProviders()
	{

		var source =
        {
			type: "GET",
			datatype: "json",
			
            datafields: [
				{ name: 'id_proveedor', type: 'integer'},
				{ name: 'codigo_proveedor', type: 'string'},
				{ name: 'nombre', type: 'string'},
				{ name: 'nombre_comercial', type: 'string'},
				{ name: 'identificacion', type: 'string'},
				{ name: 'correo', type: 'string'},
				{ name: 'telefono', type: 'string'},
				{ name: 'celular', type: 'string'},
				{ name: 'id_status', type: 'integer'},
				{ name: 'direccion', type: 'string'},

			],
            url: '/getProviders',
			cache: false
        };
		
		var productsAdapter = new $.jqx.dataAdapter(source);
		
		$("#gridProviders").jqxGrid(
        {
			width: '100%',
			source: productsAdapter,
			theme: 'ui-start',
			width: "100%",
			autoheight: "true",
			columnsresize: true,
			selectionmode: 'singlerow',
			pageable: true,
			enabletooltips: true,
			sortable: true,
			editable: false,
			showfilterrow: true,
        	filterable: true,
			enablebrowserselection: true,
			pagermode: "simple",
			pagesize: 20,
		 	altrows: true,
			columns: [
	
                  { text: 'Codigo', datafield: 'id_provider', displayfield: 'codigo_proveedor', width: "20%"},
				  { text: 'Nombre', datafield: 'nombre', displayfield: 'nombre', width: "20%"},
				  { text: 'Nombre Comercial', datafield: 'nombre_comercial', displayfield: 'nombre_comercial', width: "20%"},
				  { text: 'Identificacion', datafield: 'identificacion', displayfield: 'identificacion', width: "20%"},
				  { text: 'Telefono', datafield: 'telefono', displayfield: 'telefono', width: "20%"},
				  { text: 'Celular', datafield: 'celular', displayfield: 'celular', width: "20%"},
				  { text: 'Correo', datafield: 'correo', displayfield: 'correo', width: "20%"},
				  { text: 'Direccion', datafield: 'direccion', displayfield: 'direccion', width: "20%"},

			]
		});
	}
	
	
	
	$('#gridProviders').on('rowdoubleclick', function (event) 
	{
		clearProviderForm();
		$("#action").val(2);
		$("#txtName").val($('#gridProviders').jqxGrid('getrowdata', event.args.rowindex).nombre);
		$("#providerId").val($('#gridProviders').jqxGrid('getrowdata', event.args.rowindex).id_proveedor);
		$("#txtCode").val($('#gridProviders').jqxGrid('getrowdata', event.args.rowindex).codigo_proveedor);
		$("#txtCommercialName").val($('#gridProviders').jqxGrid('getrowdata', event.args.rowindex).nombre_comercial);
		$("#txtTelephone").val($('#gridProviders').jqxGrid('getrowdata', event.args.rowindex).telefono);
		$("#txtCellphone").val($('#gridProviders').jqxGrid('getrowdata', event.args.rowindex).celular);
		$("#txtAddress").val($('#gridProviders').jqxGrid('getrowdata', event.args.rowindex).direccion);
		$("#txtEmail").val($('#gridProviders').jqxGrid('getrowdata', event.args.rowindex).correo);
		$("#txtId").val($('#gridProviders').jqxGrid('getrowdata', event.args.rowindex).identificacion);
		$("#cboStatus").val($('#gridProviders').jqxGrid('getrowdata', event.args.rowindex).id_status);
		$("#searchSection").addClass("hidden");
		$("#formSection").removeClass("hidden");
	});
	

	function readURL(input) 
	{

	    if (input.files && input.files[0]) 
	    {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	            $('#preview').attr('src', e.target.result);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	function top() 
	{
    	//document.body.scrollTop = 0; // For Chrome, Safari and Opera
    	//document.documentElement.scrollTop = 0; // For IE and Firefox
    	$("html, body").animate({ scrollTop: 0 }, 600);
	} 	

	function ShowNotificacionError(texto, modal)
	{
		$("#wrongNotification").jqxNotification({ appendContainer: "#"+modal });
		$("#wrongNotification label").html(texto);
		$("#wrongNotification").jqxNotification("open");
	}

	function ShowNotificacionOk(texto)
	{
		$("#correctNotification label").html(texto);
		$("#correctNotification").jqxNotification("open");
	}
	function clearCategoryModal(){
		$("#id_category_modal").val("");
		$("#categoryTypeAction").val(1);
		$("#txtCatName").val("");
		$("#chkMarca").prop("checked",false); 
		$("#chkGenero").prop("checked",false);
		$("#chkColor").prop("checked",false);
		$("#chkTalla").prop("checked",false); 
		$("#chkTipoManga").prop("checked",false);
		$("#chkDimensiones").prop("checked",false);
		$("#chkPCode").prop("checked",false);
		$("#chkUMedida").prop("checked",false);
		$("#chkSerie").prop("checked",false);

	}
	function clearProviderForm()
	{
		$("#action").val(1);
		$("#providerId").val('');
		$("#txtCode").val('');
		$("#txtName").val('');
		$("#txtCommercialName").val('');
		$("#txtTelephone").val('');
		$("#txtCellphone").val('');
		$("#txtId").val('');
		$("#txtEmail").val('');
		$("#txtAddress").val('');
		$("#cboStatus").val(1);
		setTimeout(function(){ top(); }, 700);

	}

});//end ready event