
$(document).ready(function() 
{

	$("#jqxLoader").jqxLoader({ isModal: true, width: 100, height: 60, imagePosition: 'center', theme: 'orange' });
	$("#jqxLoader").css('z-index', 99999);
	var Noedit = function (row) 
	{
        return false;
	};
	$("#correctNotification").jqxNotification
	({ 
		autoOpen: false, 
		closeOnClick: true, 
		autoClose: true, 
		template: "success",
		theme: "orange",
		appendContainer: "#containtmsg",
		width: "100%"
	});
	
    $("#wrongNotification").jqxNotification
	({ 
		autoOpen: false, 
		closeOnClick: true, 
		autoClose: true, 
		template: "error",
		theme: "orange",
		appendContainer: "#containtmsg",
		width: "100%"
	});
	getrates();
	function getrates()
	{
		var source =
	        {
				type: "GET",
				datatype: "json",
	            datafields: [
					{ name: 'id_exchange_rate', type: 'integer'},
					{ name: 'date', type: 'String'},
					{ name: 'oficial_rate', type: 'number'},
					{ name: 'sale_rate', type: 'number'},

				],
	            url: 'getrates',
				cache: false
	        };
			
			var RatesAdapter = new $.jqx.dataAdapter(source);
			
			$("#gridrates").jqxGrid(
	        {
				source: RatesAdapter,
				theme: 'orange',
				width: '100%',
				rowsheight: 25,
				autoheight: "true",
				columnsresize: true,
				selectionmode: 'singlecell',
				pageable: true,
				enabletooltips: true,
				sortable: true,
				editable: true,
				showfilterrow: true,
            	filterable: true,
				enablebrowserselection: true,
				pagermode: "simple",
				pagesize: 30,
			 	altrows: true,
				columns: [
	
                { text: 'Date', datafield: 'id_exchange_rate', displayfield: 'date', width: '60%', cellbeginedit: Noedit},
				{ text: 'Official Exchange Rate', datafield: 'oficial_rate', displayfield: 'oficial_rate', width: '20%', cellsalign: 'right', cellbeginedit: Noedit},
				{ text: 'Sale Exchange Rate', datafield: 'sale_rate', displayfield: 'sale_rate', width: '20%', cellsalign: 'right'},

				]
			});	
			$("#gridrates").on('cellendedit', function (event) 
			{
			    console.log(event);
				var id_exchange_rate = event.args.row.id_exchange_rate;
				var sale_rate = event.args.value;
				if(isNumber(sale_rate))
				{
					$('#jqxLoader').jqxLoader('open');
					$.ajax({
						type: "POST",
						dataType: "json",
						url: "savesalerate",
						cache: false,
						data: { id_exchange_rate: id_exchange_rate, sale_rate:sale_rate	},
						success: function(datos)
						{
							$('#jqxLoader').jqxLoader('close');
							if(datos)
							{
								if(datos.result == "ok")
								{
									ShowNotificacionOk('El tipo de cambio de venta se guardó correctamente', "containtmsg");
								}
								else
								{
									ShowNotificacionError('Se ha producido error favor recargar la pagina e intente nuevamente', "containtmsg");
								}
							}				
						},
						error: function(datos)
						{
							$('#jqxLoader').jqxLoader('close');
							ShowNotificacionError('Error de Red!', "containtmsg");
						}
					});			  
				}
				else
				{
					ShowNotificacionError('Solo valores numericos!', "containtmsg");
				}
			});
	}
	function ShowNotificacionError(texto, modal)
	{
		$("#wrongNotification").jqxNotification({ appendContainer: "#"+modal });
		$("#wrongNotification label").html(texto);
		$("#wrongNotification").jqxNotification("open");
	}

	function ShowNotificacionOk(texto)
	{
		$("#correctNotification label").html(texto);
		$("#correctNotification").jqxNotification("open");
	}
	function isNumber(n) 
	{
  		return !isNaN(parseFloat(n)) && isFinite(n);
	}


});
