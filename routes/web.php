<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('getroles', 'Controller@getRoles');
Route::get('/usuarios', 'UsersController@index');
Route::post('/getusers', 'UsersController@getUsers');
Route::post('/getuserrol', 'Controller@getUserRol');
Route::post('/saveuser', 'UsersController@saveUser');
Route::post('/updateuser', 'UsersController@updateUser');
Route::get('/inventario', 'InventoryController@index');
Route::get('/getPrdCategories', 'InventoryController@getPrdCategories');
Route::post('/saveCategory', 'InventoryController@saveCategory');
Route::post('/updateCategory', 'InventoryController@updateCategory');
Route::get('/getMarcas', 'InventoryController@getMarcas');
Route::get('/getMangas', 'InventoryController@getMangas');
Route::get('/getTallas', 'InventoryController@getTallas');
Route::get('/getMeasures', 'InventoryController@getMeasures');
Route::get('/getTipos', 'InventoryController@getTipos');
Route::get('/getColores', 'InventoryController@getColores');
Route::get('/getGeneros', 'InventoryController@getGeneros');
Route::get('/getTelas', 'InventoryController@getTelas');
Route::get('/getDimensiones', 'InventoryController@getDimensiones');
Route::post('/saveManga', 'InventoryController@saveManga');
Route::post('/saveMarca', 'InventoryController@saveMarca');
Route::post('/saveTipo', 'InventoryController@saveTipo');
Route::post('/saveTalla', 'InventoryController@saveTalla');
Route::post('/saveMeasure', 'InventoryController@saveMeasure');
Route::post('/saveColor', 'InventoryController@saveColor');
Route::post('/saveGenero', 'InventoryController@saveGenero');
Route::post('/saveDimension', 'InventoryController@saveDimension');
Route::post('/saveTela', 'InventoryController@saveTela');
Route::post('/saveProduct', 'InventoryController@saveProduct');
Route::post('/updateProduct', 'InventoryController@updateProduct');
Route::get('/getProducts', 'InventoryController@getProducts');
Route::get('/downloadProducts', 'InventoryController@downloadProducts');
Route::get('/proveedores', 'ProviderController@index');
Route::post('/saveProvider', 'ProviderController@saveProvider');
Route::post('/updateProvider', 'ProviderController@updateProvider');
Route::get('/getProviders', 'ProviderController@getProviders');
Route::get('/billing', 'BillingController@index');
Route::post("/saveCustomerBilling", "Controller@saveCustomerBilling");
Route::get("/getCustomers", "Controller@getCustomers");
Route::get('/getServices', 'InventoryController@getServices');
Route::post('/saveBill', 'InventoryController@saveBill');
Route::get('/getBills', 'InventoryController@getBills');
Route::post('/getDetails', 'InventoryController@getDetails');