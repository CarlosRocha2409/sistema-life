-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-01-2021 a las 06:03:58
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `life`
--

DELIMITER $$
--
-- Funciones
--
CREATE DEFINER=`root`@`localhost` FUNCTION `getCodeBill` (`_id_tipo_inv` INT) RETURNS VARCHAR(10) CHARSET latin1 begin
    set @new_id = (select case when count(codigo_documento) = 0 then '0000000001' else lpad(max(codigo_documento)+1,10, 0)  end from documentos_inventario where id_tipo_inv = _id_tipo_inv);
  return @new_id;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL,
  `codigo_cliente` varchar(20) NOT NULL,
  `nombre_cliente` varchar(150) NOT NULL,
  `no_id` varchar(20) NOT NULL,
  `direccion` varchar(400) NOT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `telefono` varchar(8) DEFAULT NULL,
  `celular` varchar(8) DEFAULT NULL,
  `celular2` varchar(8) DEFAULT NULL,
  `id_status` int(1) DEFAULT 1,
  `id_user_created` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `codigo_cliente`, `nombre_cliente`, `no_id`, `direccion`, `correo`, `telefono`, `celular`, `celular2`, `id_status`, `id_user_created`, `date_created`) VALUES
(1, '123456', 'test', '123465789', 'test address', 'test@test.com', '12345678', '12345678', '12345678', 1, 1, '2021-01-03 03:27:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colores`
--

CREATE TABLE `colores` (
  `id_color` int(11) NOT NULL,
  `color` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `colores`
--

INSERT INTO `colores` (`id_color`, `color`) VALUES
(1, 'Azul');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_documentos_inventario`
--

CREATE TABLE `detalle_documentos_inventario` (
  `id_detalle_documento` int(11) NOT NULL,
  `id_documento` int(11) NOT NULL,
  `id_bodega` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` decimal(18,2) DEFAULT 0.00,
  `costo_promedio` decimal(18,2) DEFAULT 0.00,
  `total_costo` decimal(18,2) DEFAULT 0.00,
  `precio` decimal(18,2) DEFAULT 0.00,
  `subtotal` decimal(18,2) DEFAULT 0.00,
  `impuesto` decimal(18,2) DEFAULT 0.00,
  `total_neto` decimal(18,2) DEFAULT 0.00,
  `id_user_created` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `id_user_update` int(11) DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `is_apply_inv` bit(1) DEFAULT NULL,
  `id_servicio` int(11) DEFAULT NULL,
  `costo_servicio` decimal(18,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_kardex`
--

CREATE TABLE `detalle_kardex` (
  `id_kardex` int(11) NOT NULL,
  `id_bodega` int(11) NOT NULL,
  `id_tipo_inv` int(11) NOT NULL,
  `id_documento_inv` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `cantidad` decimal(18,2) NOT NULL,
  `saldo` decimal(18,2) DEFAULT NULL,
  `costo_promedio` decimal(18,2) DEFAULT NULL,
  `costo_promedio_dolar` decimal(18,2) DEFAULT NULL,
  `total_costo` decimal(18,2) NOT NULL,
  `total_costo_dolar` decimal(18,2) DEFAULT NULL,
  `saldo_costo` decimal(18,2) DEFAULT NULL,
  `saldo_costo_dolar` decimal(18,2) DEFAULT NULL,
  `id_user_created` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `is_sync` bit(1) DEFAULT b'0',
  `date_sync` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dimensiones`
--

CREATE TABLE `dimensiones` (
  `id_dimension` int(11) NOT NULL,
  `dimension` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `dimensiones`
--

INSERT INTO `dimensiones` (`id_dimension`, `dimension`) VALUES
(1, '3X3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos_inventario`
--

CREATE TABLE `documentos_inventario` (
  `id_documento` int(11) NOT NULL,
  `id_tipo_inv` int(11) NOT NULL,
  `id_bodega_out` int(11) DEFAULT NULL,
  `id_bodega_input` int(11) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_vendedor` int(11) DEFAULT NULL,
  `codigo_documento` varchar(10) NOT NULL,
  `serie` varchar(5) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `total_items` decimal(18,2) DEFAULT 0.00,
  `total_costo` decimal(18,2) DEFAULT 0.00,
  `subtotal` decimal(18,2) DEFAULT 0.00,
  `total_impuesto` decimal(18,2) DEFAULT 0.00,
  `total_neto` decimal(18,2) DEFAULT 0.00,
  `descripcion` varchar(500) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `factura_cxp` varchar(20) DEFAULT NULL,
  `id_tipo_origen` int(11) DEFAULT NULL,
  `id_documento_origen` int(11) DEFAULT NULL,
  `id_user_created` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `id_user_update` int(11) DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `id_user_apply` int(11) DEFAULT NULL,
  `date_apply` datetime DEFAULT NULL,
  `id_user_anul` int(11) DEFAULT NULL,
  `date_anul` datetime DEFAULT NULL,
  `id_anulacion` int(11) DEFAULT NULL,
  `concepto_anul` varchar(500) DEFAULT NULL,
  `is_anul` bit(1) DEFAULT b'0',
  `is_apply_inv` bit(1) DEFAULT b'0',
  `is_apply_cxc` bit(1) DEFAULT b'0',
  `is_apply_cxp` bit(1) DEFAULT b'0',
  `is_exonerada` bit(1) DEFAULT b'0',
  `is_sync` bit(1) DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id_empresa` int(11) NOT NULL,
  `empresa` varchar(100) NOT NULL,
  `ruc` varchar(50) DEFAULT NULL,
  `logotipo` varchar(100) DEFAULT NULL,
  `contrasena_anulacion` varchar(20) DEFAULT NULL,
  `contrasena_descuento` varchar(20) DEFAULT NULL,
  `contrasena_exonerado` varchar(20) DEFAULT NULL,
  `contrasena_cerrarcaja` varchar(20) DEFAULT NULL,
  `is_active` bit(1) DEFAULT b'1',
  `montocierrecaja` decimal(18,2) DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresas`
--

INSERT INTO `empresas` (`id_empresa`, `empresa`, `ruc`, `logotipo`, `contrasena_anulacion`, `contrasena_descuento`, `contrasena_exonerado`, `contrasena_cerrarcaja`, `is_active`, `montocierrecaja`) VALUES
(1, 'Ferreteria Rosita', '111-111111-1111X', 'images/Artboard.png', '123', '123', '123', '123', b'1', '2500.00'),
(2, 'Tienda de Ropa', '', NULL, '', '', '', '', b'1', '0.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forms`
--

CREATE TABLE `forms` (
  `id_form` int(11) NOT NULL,
  `id_father_form` int(11) DEFAULT NULL,
  `form` varchar(50) NOT NULL,
  `file_name` varchar(50) DEFAULT NULL,
  `Level` int(11) NOT NULL,
  `version` int(2) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `forms`
--

INSERT INTO `forms` (`id_form`, `id_father_form`, `form`, `file_name`, `Level`, `version`, `icon`) VALUES
(1, NULL, 'Usuarios', NULL, 1, 2, 'fa fa-users'),
(2, 1, 'usuarios', 'usuarios', 2, 2, 'fa fa-users'),
(3, NULL, 'Productos', 'productos', 1, 2, 'fa fa-gear'),
(4, 3, 'Inventario', 'inventario', 2, 2, 'fa fa-inventory'),
(5, 3, 'Proveedores', 'proveedores', 2, 2, 'fa fa-briefcase'),
(6, 3, 'Facturacion', 'billing', 2, 2, 'fa-fa-shopping-bag');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forms_options`
--

CREATE TABLE `forms_options` (
  `id_option` int(11) NOT NULL,
  `id_form` int(11) NOT NULL,
  `code_option` varchar(50) NOT NULL,
  `name_option` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `forms_options`
--

INSERT INTO `forms_options` (`id_option`, `id_form`, `code_option`, `name_option`) VALUES
(1, 4, 'btnFind', 'Buscar'),
(2, 4, 'btnSave', 'Guardar'),
(3, 4, 'btnNew', 'Nuevo'),
(4, 4, 'btnDownload', 'Descargar'),
(5, 5, 'btnFind', 'Buscar'),
(6, 5, 'btnNew', 'Nuevo'),
(7, 5, 'btnSave', 'Guardar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `generos`
--

CREATE TABLE `generos` (
  `id_genero` int(11) NOT NULL,
  `genero` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `generos`
--

INSERT INTO `generos` (`id_genero`, `genero`) VALUES
(1, 'Femenino'),
(2, 'Masculino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventory`
--

CREATE TABLE `inventory` (
  `id_product` int(11) NOT NULL,
  `product_code` varchar(20) NOT NULL,
  `optional_code1` varchar(20) DEFAULT NULL,
  `optional_code2` varchar(20) DEFAULT NULL,
  `product_name` varchar(100) NOT NULL,
  `id_category` int(11) NOT NULL,
  `id_status` int(1) NOT NULL DEFAULT 1,
  `product_description` varchar(200) NOT NULL,
  `id_marca` int(11) DEFAULT NULL,
  `id_dimension` int(11) DEFAULT NULL,
  `id_genero` int(11) DEFAULT NULL,
  `id_talla` int(11) DEFAULT NULL,
  `id_tela` int(11) DEFAULT NULL,
  `id_color` int(11) DEFAULT NULL,
  `id_tipo_manga` int(11) DEFAULT NULL,
  `provider_code` varchar(20) DEFAULT NULL,
  `id_unidad_medida` int(11) DEFAULT NULL,
  `numero_serie` varchar(25) DEFAULT NULL,
  `image` varchar(150) DEFAULT NULL,
  `id_user_created` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `id_user_updated` int(11) DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

CREATE TABLE `marcas` (
  `id_marca` int(11) NOT NULL,
  `marca` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`id_marca`, `marca`) VALUES
(1, 'Tricotextil');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_000000_create_users_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product_categories`
--

CREATE TABLE `product_categories` (
  `id_category` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `marca` int(1) NOT NULL DEFAULT 0,
  `genero` int(1) NOT NULL DEFAULT 0,
  `color` int(1) NOT NULL DEFAULT 0,
  `talla` int(1) NOT NULL DEFAULT 0,
  `tipo` int(1) NOT NULL DEFAULT 0,
  `tela` int(1) NOT NULL DEFAULT 0,
  `tipo_manga` int(1) NOT NULL DEFAULT 0,
  `dimensiones` int(1) NOT NULL DEFAULT 0,
  `codigo_proveedor` int(1) NOT NULL DEFAULT 0,
  `unidad_medida` int(1) NOT NULL DEFAULT 0,
  `serie` int(1) NOT NULL DEFAULT 0,
  `id_user_created` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `id_user_updated` int(11) DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `product_categories`
--

INSERT INTO `product_categories` (`id_category`, `nombre`, `marca`, `genero`, `color`, `talla`, `tipo`, `tela`, `tipo_manga`, `dimensiones`, `codigo_proveedor`, `unidad_medida`, `serie`, `id_user_created`, `date_created`, `id_user_updated`, `date_updated`, `is_active`) VALUES
(1, 'Camisa', 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, '2021-01-03 15:40:22', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id_proveedor` int(11) NOT NULL,
  `codigo_proveedor` varchar(20) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `nombre_comercial` varchar(150) NOT NULL,
  `identificacion` varchar(20) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `celular` varchar(20) DEFAULT NULL,
  `direccion` varchar(300) DEFAULT NULL,
  `id_status` int(11) DEFAULT NULL,
  `id_user_created` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `id_user_updated` int(11) DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `codigo_proveedor`, `nombre`, `nombre_comercial`, `identificacion`, `correo`, `telefono`, `celular`, `direccion`, `id_status`, `id_user_created`, `date_created`, `id_user_updated`, `date_updated`) VALUES
(2, '12345678', 'proveedor uno', 'prov_uno', '0000000000000', 'correo@correo.com', '22223333', '88887777', 'test address', 1, 1, '2021-01-03 00:16:11', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id_rol` int(11) NOT NULL,
  `rol` varchar(50) NOT NULL,
  `status` bit(1) DEFAULT b'1',
  `date_created` datetime NOT NULL,
  `date_down` datetime DEFAULT NULL,
  `page` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id_rol`, `rol`, `status`, `date_created`, `date_down`, `page`) VALUES
(1, 'Administrador', b'1', '2020-04-29 00:00:00', NULL, 'home'),
(2, 'Ejecutivo de ventas', b'1', '2020-04-29 00:00:00', NULL, 'Productos'),
(3, 'Ejecutivo de ventas - Solo lectura', b'1', '2020-04-29 00:00:00', NULL, 'productos'),
(4, 'Asistente Administrativo', b'1', '2020-05-29 00:00:00', NULL, NULL),
(5, 'Asistente Administrativo - Solo lectura', b'1', '2020-05-24 00:00:00', NULL, NULL),
(6, 'Responsable de bodega', b'1', '2020-05-24 00:00:00', NULL, NULL),
(7, 'Responsable de bodega - Solo lectura', b'1', '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_forms`
--

CREATE TABLE `roles_forms` (
  `id_rol_form` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `id_form` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles_forms`
--

INSERT INTO `roles_forms` (`id_rol_form`, `id_rol`, `id_form`) VALUES
(573, 1, 1),
(574, 1, 2),
(575, 1, 3),
(576, 1, 4),
(577, 1, 5),
(578, 1, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_forms_options`
--

CREATE TABLE `roles_forms_options` (
  `id_rol_option` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `id_form` int(11) NOT NULL,
  `id_option` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles_forms_options`
--

INSERT INTO `roles_forms_options` (`id_rol_option`, `id_rol`, `id_form`, `id_option`) VALUES
(1, 1, 4, 1),
(2, 1, 4, 2),
(3, 1, 4, 3),
(4, 1, 4, 4),
(5, 1, 5, 5),
(6, 1, 5, 6),
(7, 1, 5, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `id_servicio` int(11) NOT NULL,
  `servicio` varchar(150) NOT NULL,
  `id_status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`id_servicio`, `servicio`, `id_status`) VALUES
(1, 'bordado', 1),
(2, 'servicio 2', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tallas`
--

CREATE TABLE `tallas` (
  `id_talla` int(11) NOT NULL,
  `talla` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tallas`
--

INSERT INTO `tallas` (`id_talla`, `talla`) VALUES
(1, 'XS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telas`
--

CREATE TABLE `telas` (
  `id_tela` int(11) NOT NULL,
  `tela` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `telas`
--

INSERT INTO `telas` (`id_tela`, `tela`) VALUES
(1, 'Algodon 100%');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE `tipo` (
  `id_tipo` int(11) NOT NULL,
  `tipo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_mangas`
--

CREATE TABLE `tipo_mangas` (
  `id_tipo_manga` int(11) NOT NULL,
  `tipo_manga` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_mangas`
--

INSERT INTO `tipo_mangas` (`id_tipo_manga`, `tipo_manga`) VALUES
(1, 'Manga Corta');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidad_medida`
--

CREATE TABLE `unidad_medida` (
  `id_unidad_medida` int(11) NOT NULL,
  `unidad_medida` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `unidad_medida`
--

INSERT INTO `unidad_medida` (`id_unidad_medida`, `unidad_medida`) VALUES
(1, 'Unidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `direccion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cargo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `nota_baja` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_img` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `direccion`, `telefono`, `cargo`, `fecha_ingreso`, `status`, `nota_baja`, `url_img`) VALUES
(1, 'Administrador', 'admin', 'admin@life.com.ni', '2020-04-27 19:21:09', '$2y$10$EprvYTgv6z4sNuhs/gZ29eYAhVCDDftUsop2qkrr6XCJM1v.pgKe6', 'aCVHCSIZghEfxPWfWVtzjCuDXfTlWaFa2eG88QZ3vD7ciaVigSQ6IWzbWm9K', '2020-04-27 19:21:09', '2020-08-01 11:09:10', 'test address', '88487480', '1', '2020-05-04', 1, NULL, 'users/admin/19052020002403.jpeg'),
(2, 'empleado1', 'user1', 'user1@life.com.ni', NULL, '$2y$10$VSw1A.VonpK9azYsc/Zwh.n/ALteloF203YfhE8eDR/Z5.NJCab1G', NULL, '2020-05-04 03:29:29', '2020-05-26 05:17:15', 'test', '12345678', '4', NULL, 0, 'test2', NULL),
(5, 'test2', 'test2', 'test2@life.com.ni', NULL, '$2y$10$hU6uDOrwR9bywwTJ3z7D0uoo4KYwpTip.tuPPHEXByuaRedu0blou', NULL, '2020-05-19 02:36:23', '2020-05-19 02:36:23', 'tst', '12345678', '3', '2020-05-18', 1, NULL, 'users/test2/18052020203623.jpeg'),
(6, 'test3', 'test3', 'test3@test.com', NULL, '$2y$10$hyBk1DXWe9p0ZglqsPNNA.4O1CLQda0ridRh5S56XZgyUyDJgf8U.', NULL, '2020-05-24 14:56:55', '2020-05-24 14:56:55', 'figvgviygv  hgydtrd', '12345678', '1', '2020-05-24', 1, NULL, 'users/test3/24052020085655.jpeg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_actions`
--

CREATE TABLE `users_actions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(100) NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_roles`
--

CREATE TABLE `users_roles` (
  `id_user_rol` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users_roles`
--

INSERT INTO `users_roles` (`id_user_rol`, `id_user`, `id_rol`) VALUES
(5, 5, 6),
(6, 5, 4),
(7, 5, 2),
(8, 6, 1),
(9, 6, 2),
(10, 2, 6),
(11, 1, 1),
(12, 1, 2);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vw_inventory`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vw_inventory` (
`id_product` int(11)
,`product_code` varchar(20)
,`optional_code1` varchar(20)
,`optional_code2` varchar(20)
,`product_name` varchar(100)
,`id_category` int(11)
,`category` varchar(20)
,`id_status` int(1)
,`status` varchar(8)
,`product_description` varchar(200)
,`id_marca` int(11)
,`marca` varchar(100)
,`id_dimension` int(11)
,`dimension` varchar(100)
,`id_genero` int(11)
,`genero` varchar(100)
,`id_talla` int(11)
,`talla` varchar(100)
,`id_tela` int(11)
,`tela` varchar(100)
,`id_color` int(11)
,`color` varchar(100)
,`id_tipo_manga` int(11)
,`tipo_manga` varchar(100)
,`provider_code` varchar(20)
,`id_unidad_medida` int(11)
,`unidad_medida` varchar(100)
,`numero_serie` varchar(25)
,`image` varchar(150)
,`id_user_created` int(11)
,`user_created` varchar(100)
,`date_created` datetime
,`id_user_updated` int(11)
,`user_updated` varchar(100)
,`date_updated` datetime
);

-- --------------------------------------------------------

--
-- Estructura para la vista `vw_inventory`
--
DROP TABLE IF EXISTS `vw_inventory`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_inventory`  AS  select `inventory`.`id_product` AS `id_product`,`inventory`.`product_code` AS `product_code`,`inventory`.`optional_code1` AS `optional_code1`,`inventory`.`optional_code2` AS `optional_code2`,`inventory`.`product_name` AS `product_name`,`inventory`.`id_category` AS `id_category`,(select `product_categories`.`nombre` from `product_categories` where `product_categories`.`id_category` = `inventory`.`id_category`) AS `category`,`inventory`.`id_status` AS `id_status`,case `inventory`.`id_status` when 1 then 'Activo' else 'Inactivo' end AS `status`,`inventory`.`product_description` AS `product_description`,`inventory`.`id_marca` AS `id_marca`,(select `marcas`.`marca` from `marcas` where `marcas`.`id_marca` = `inventory`.`id_marca`) AS `marca`,`inventory`.`id_dimension` AS `id_dimension`,(select `dimensiones`.`dimension` from `dimensiones` where `dimensiones`.`id_dimension` = `inventory`.`id_dimension`) AS `dimension`,`inventory`.`id_genero` AS `id_genero`,(select `generos`.`genero` from `generos` where `generos`.`id_genero` = `inventory`.`id_genero`) AS `genero`,`inventory`.`id_talla` AS `id_talla`,(select `tallas`.`talla` from `tallas` where `tallas`.`id_talla` = `inventory`.`id_talla`) AS `talla`,`inventory`.`id_tela` AS `id_tela`,(select `telas`.`tela` from `telas` where `telas`.`id_tela` = `inventory`.`id_tela`) AS `tela`,`inventory`.`id_color` AS `id_color`,(select `colores`.`color` from `colores` where `colores`.`id_color` = `inventory`.`id_color`) AS `color`,`inventory`.`id_tipo_manga` AS `id_tipo_manga`,(select `tipo_mangas`.`tipo_manga` from `tipo_mangas` where `tipo_mangas`.`id_tipo_manga` = `inventory`.`id_tipo_manga`) AS `tipo_manga`,`inventory`.`provider_code` AS `provider_code`,`inventory`.`id_unidad_medida` AS `id_unidad_medida`,(select `unidad_medida`.`unidad_medida` from `unidad_medida` where `unidad_medida`.`id_unidad_medida` = `inventory`.`id_unidad_medida`) AS `unidad_medida`,`inventory`.`numero_serie` AS `numero_serie`,`inventory`.`image` AS `image`,`inventory`.`id_user_created` AS `id_user_created`,(select `users`.`name` from `users` where `users`.`id` = `inventory`.`id_user_created`) AS `user_created`,`inventory`.`date_created` AS `date_created`,`inventory`.`id_user_updated` AS `id_user_updated`,(select `users`.`name` from `users` where `users`.`id` = `inventory`.`id_user_updated`) AS `user_updated`,`inventory`.`date_updated` AS `date_updated` from `inventory` ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `colores`
--
ALTER TABLE `colores`
  ADD PRIMARY KEY (`id_color`);

--
-- Indices de la tabla `detalle_documentos_inventario`
--
ALTER TABLE `detalle_documentos_inventario`
  ADD PRIMARY KEY (`id_detalle_documento`);

--
-- Indices de la tabla `detalle_kardex`
--
ALTER TABLE `detalle_kardex`
  ADD PRIMARY KEY (`id_kardex`);

--
-- Indices de la tabla `dimensiones`
--
ALTER TABLE `dimensiones`
  ADD PRIMARY KEY (`id_dimension`);

--
-- Indices de la tabla `documentos_inventario`
--
ALTER TABLE `documentos_inventario`
  ADD PRIMARY KEY (`id_documento`);

--
-- Indices de la tabla `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id_form`),
  ADD KEY `idx_form_father` (`id_father_form`);

--
-- Indices de la tabla `forms_options`
--
ALTER TABLE `forms_options`
  ADD PRIMARY KEY (`id_option`),
  ADD KEY `idx_option_form` (`id_form`);

--
-- Indices de la tabla `generos`
--
ALTER TABLE `generos`
  ADD PRIMARY KEY (`id_genero`);

--
-- Indices de la tabla `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id_product`);

--
-- Indices de la tabla `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`id_marca`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id_category`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `roles_forms`
--
ALTER TABLE `roles_forms`
  ADD PRIMARY KEY (`id_rol_form`),
  ADD KEY `id_rol` (`id_rol`),
  ADD KEY `id_form` (`id_form`);

--
-- Indices de la tabla `roles_forms_options`
--
ALTER TABLE `roles_forms_options`
  ADD PRIMARY KEY (`id_rol_option`),
  ADD KEY `id_rol` (`id_rol`),
  ADD KEY `id_form` (`id_form`),
  ADD KEY `id_option` (`id_option`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id_servicio`);

--
-- Indices de la tabla `tallas`
--
ALTER TABLE `tallas`
  ADD PRIMARY KEY (`id_talla`);

--
-- Indices de la tabla `telas`
--
ALTER TABLE `telas`
  ADD PRIMARY KEY (`id_tela`);

--
-- Indices de la tabla `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id_tipo`);

--
-- Indices de la tabla `tipo_mangas`
--
ALTER TABLE `tipo_mangas`
  ADD PRIMARY KEY (`id_tipo_manga`);

--
-- Indices de la tabla `unidad_medida`
--
ALTER TABLE `unidad_medida`
  ADD PRIMARY KEY (`id_unidad_medida`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `users_actions`
--
ALTER TABLE `users_actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Users_Actions_fk` (`user_id`);

--
-- Indices de la tabla `users_roles`
--
ALTER TABLE `users_roles`
  ADD PRIMARY KEY (`id_user_rol`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_rol` (`id_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `colores`
--
ALTER TABLE `colores`
  MODIFY `id_color` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `detalle_documentos_inventario`
--
ALTER TABLE `detalle_documentos_inventario`
  MODIFY `id_detalle_documento` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalle_kardex`
--
ALTER TABLE `detalle_kardex`
  MODIFY `id_kardex` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dimensiones`
--
ALTER TABLE `dimensiones`
  MODIFY `id_dimension` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `documentos_inventario`
--
ALTER TABLE `documentos_inventario`
  MODIFY `id_documento` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `forms`
--
ALTER TABLE `forms`
  MODIFY `id_form` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `forms_options`
--
ALTER TABLE `forms_options`
  MODIFY `id_option` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `generos`
--
ALTER TABLE `generos`
  MODIFY `id_genero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id_product` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `marcas`
--
ALTER TABLE `marcas`
  MODIFY `id_marca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `roles_forms`
--
ALTER TABLE `roles_forms`
  MODIFY `id_rol_form` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=579;

--
-- AUTO_INCREMENT de la tabla `roles_forms_options`
--
ALTER TABLE `roles_forms_options`
  MODIFY `id_rol_option` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id_servicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tallas`
--
ALTER TABLE `tallas`
  MODIFY `id_talla` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `telas`
--
ALTER TABLE `telas`
  MODIFY `id_tela` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tipo`
--
ALTER TABLE `tipo`
  MODIFY `id_tipo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_mangas`
--
ALTER TABLE `tipo_mangas`
  MODIFY `id_tipo_manga` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `unidad_medida`
--
ALTER TABLE `unidad_medida`
  MODIFY `id_unidad_medida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `users_actions`
--
ALTER TABLE `users_actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users_roles`
--
ALTER TABLE `users_roles`
  MODIFY `id_user_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `forms_options`
--
ALTER TABLE `forms_options`
  ADD CONSTRAINT `forms_options_ibfk_1` FOREIGN KEY (`id_form`) REFERENCES `forms` (`id_form`);

--
-- Filtros para la tabla `roles_forms`
--
ALTER TABLE `roles_forms`
  ADD CONSTRAINT `roles_forms_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`),
  ADD CONSTRAINT `roles_forms_ibfk_2` FOREIGN KEY (`id_form`) REFERENCES `forms` (`id_form`);

--
-- Filtros para la tabla `roles_forms_options`
--
ALTER TABLE `roles_forms_options`
  ADD CONSTRAINT `roles_forms_options_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`),
  ADD CONSTRAINT `roles_forms_options_ibfk_2` FOREIGN KEY (`id_form`) REFERENCES `forms` (`id_form`),
  ADD CONSTRAINT `roles_forms_options_ibfk_3` FOREIGN KEY (`id_option`) REFERENCES `forms_options` (`id_option`);

--
-- Filtros para la tabla `users_actions`
--
ALTER TABLE `users_actions`
  ADD CONSTRAINT `Users_Actions_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `users_roles`
--
ALTER TABLE `users_roles`
  ADD CONSTRAINT `users_roles_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `users_roles_ibfk_2` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
