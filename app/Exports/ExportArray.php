<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;

class ExportArray implements FromArray
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $excelData;
    public function __construct(array $excelData)
    {
        $this->excelData = $excelData;
    }
    public function array(): array
    {
        return $this->excelData;
    }
}
