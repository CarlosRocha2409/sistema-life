<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use Carbon\Carbon;
use App\Exports\ExportArray;
use Maatwebsite\Excel\Facades\Excel;

class InventoryController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
    	session(['id_user'=> Auth::User()->id]);
        session(['username'=> Auth::User()->username]);
        session(['name'=> Auth::User()->username]);
        $html=Controller::GetMenu();
        session(['menu'=>$html]);
        $var=Controller::GetFormOptions(session('id_user'),$request->path());
        return view('inventory',['options'=>$var]);
    }
    public function getPrdCategories(){
        return response(DB::table("product_categories")->where("is_active",1)->get(), 200);
    }
    public function saveCategory(Request $rq){
        $data = $rq->except("id_category");
        $data["id_user_created"]=session("id_user");
        $data["date_created"]=Carbon::now();
        DB::table("product_categories")->insert($data);
        return response(["resultado"=>"OK"],200);
    }
    public function updateCategory(Request $rq){
        $id = $rq->input("id_category");
        $data = $rq->except("id_category");
        $data["id_user_updated"]=session("id_user");
        $data["date_updated"]=Carbon::now();
        DB::table("product_categories")->where("id_category", $id)->update($data);
        return response(["resultado"=>"OK"],200);
    }
    public function saveProduct(Request $rq){
        $picture = $rq->input('image');
        $data = $rq->all();
        $data["id_user_created"]=session("id_user");
        $data["date_created"] = Carbon::now();
        if ($picture)
        {   
            $url = $this->export_image('inventory/'.$data['product_code'], $picture);
            $data['image']=$url;
        }
        DB::table("inventory")->insert($data);
        return response(["resultado"=>"OK"],200);
    }
    public function updateProduct(Request $rq){
        $picture = $rq->input('image');
        $id = $rq->input('id_product');
        $data = $rq->except('id_product');
        $data["id_user_updated"]=session("id_user");
        $data["date_updated"] = Carbon::now();
        if ($picture)
        {   
            $url = $this->export_image('inventory/'.$data['product_code'], $picture);
            $data['image']=$url;
        }
        DB::table("inventory")->where('id_product', $id)->update($data);
        return response(["resultado"=>"OK"],200);
    }
    public function downloadProducts(){
         $data = json_decode(json_encode(DB::table("vw_inventory")->get()), true);
         return Excel::download(new ExportArray($data), 'Productos.xlsx');
    }
    public function getProducts(){
        return response(DB::table("vw_inventory")->get(), 200);
    }
    public function getMangas(){
        return response(DB::table("tipo_mangas")->get(),200);
    }
    public function getTelas(){
        return response(DB::table("telas")->get(),200);
    }
    public function getTallas(){
        return response(DB::table("tallas")->get(),200);
    }
    public function getMeasures(){
        return response(DB::table("unidad_medida")->get(),200);
    }
    public function getTipos(){
        return response(DB::table("tipo")->get(),200);
    }
    public function getColores(){
        return response(DB::table("colores")->get(),200);
    }
    public function getGeneros(){
        return response(DB::table("generos")->get(),200);
    }
    public function getMarcas(){
        return response(DB::table("marcas")->get(),200);
    }
    public function getDimensiones(){
        return response(DB::table("dimensiones")->get(),200);
    }
    public function saveManga(Request $rq){
        $value = $rq->input("value");
        DB::table("tipo_mangas")->insert(["tipo_manga"=>$value]);
        return response(["resultado"=>"OK"],200);
    }
    public function saveMarca(Request $rq){
        $value = $rq->input("value");
        DB::table("marcas")->insert(["marca"=>$value]);
        return response(["resultado"=>"OK"],200);
    }
    public function saveTipo(Request $rq){
        $value = $rq->input("value");
        DB::table("tipo")->insert(["tipo"=>$value]);
        return response(["resultado"=>"OK"],200);
    }
    public function saveTalla(Request $rq){
        $value = $rq->input("value");
        DB::table("tallas")->insert(["talla"=>$value]);
        return response(["resultado"=>"OK"],200);
    }
    public function saveMeasure(Request $rq){
        $value = $rq->input("value");
        DB::table("unidad_medida")->insert(["unidad_medida"=>$value]);
        return response(["resultado"=>"OK"],200);
    }
    public function saveColor(Request $rq){
        $value = $rq->input("value");
        DB::table("colores")->insert(["color"=>$value]);
        return response(["resultado"=>"OK"],200);
    }
    public function saveGenero(Request $rq){
        $value = $rq->input("value");
        DB::table("generos")->insert(["genero"=>$value]);
        return response(["resultado"=>"OK"],200);
    }
    public function saveDimension(Request $rq){
        $value = $rq->input("value");
        DB::table("dimensiones")->insert(["dimension"=>$value]);
        return response(["resultado"=>"OK"],200);
    }
    public function saveTela(Request $rq){
        $value = $rq->input("value");
        DB::table("telas")->insert(["tela"=>$value]);
        return response(["resultado"=>"OK"],200);
    }
    public function getServices(){
        return response(DB::table("servicios")->where("id_status",1)->get(),200);
    }
    /*
            1 - compras locales
            2 - saldo inicial
            3 - importacion
            4 - cotizacion
            5 - factura 
    */
    public function saveBill(Request $rq){
        $detail = $rq->input("detail");
        $data = $rq->except("detail");
        $code = DB::select("select getCodeBill(".$data["id_type"].") as code;");
        $doc=DB::table("documentos_inventario")->insertGetId([
            "codigo_documento"=>$code[0]->code,
            "id_tipo_inv"=>$data["id_type"],
            "id_cliente"=>$data["id_customer"],
            "id_proveedor"=>$data["id_proveedor"],
            "fecha"=>Carbon::now(),
            "total_items"=>$data["cantidad"],
            "subtotal"=>$data["subtotal"],
            "total_impuesto"=>$data["iva"],
            "total_neto"=>$data["total"],
            "id_user_created"=>Auth::User()->id,
            "date_created"=>Carbon::now()
        ], "id_documento");
        foreach ($detail as $key => $value) {
            DB::table("detalle_documentos_inventario")->insert([
                "id_documento"=>$doc,
                "id_bodega"=>1,
                "id_producto"=>$value["id_product"],
                "cantidad"=>$value["quantity"],
                "precio"=>$value["price"],
                "subtotal"=>floatval($value["total"])-floatval($value["iva"]),
                "impuesto"=>$value["iva"],
                "total_neto"=>$value["total"],
                "id_user_created"=>Auth::User()->id,
                "date_created"=>Carbon::now()
            ]);
        }

        return response(["resultado"=>"OK"],200);
    }
    public function getBills(){
        return response(DB::select("select id_documento, id_tipo_inv, id_bodega_out, id_bodega_input, id_proveedor, id_cliente, id_vendedor, codigo_documento, (select clientes.nombre_cliente from clientes where clientes.id_cliente = documentos_inventario.id_cliente) cliente, serie, fecha, total_items, total_costo, subtotal, total_impuesto, total_neto, descripcion, foto, factura_cxp, id_tipo_origen, id_documento_origen, id_user_created, date_created, id_user_update, date_update, id_user_apply, date_apply, id_user_anul, date_anul, id_anulacion, concepto_anul, is_anul, is_apply_inv, is_apply_cxc, is_apply_cxp, is_exonerada, is_sync from documentos_inventario where id_tipo_inv in (4,5);"),200);
    }
    public function getDetails(Request $rq){
        return response(DB::select("select (select inventory.product_name from inventory where inventory.id_product = detalle_documentos_inventario.id_producto) product_name, id_servicio, (select servicios.servicio from servicios where servicios.id_servicio = detalle_documentos_inventario.id_servicio ) service, id_detalle_documento, id_documento, id_bodega, id_producto id_product, cantidad quantity, costo_promedio, total_costo, precio price, subtotal, impuesto iva, total_neto total, id_user_created, date_created, id_user_update, date_update, is_apply_inv from detalle_documentos_inventario where id_documento=:id_documento;",["id_documento"=>$rq->input("id_documento")]),200);
    }
}
