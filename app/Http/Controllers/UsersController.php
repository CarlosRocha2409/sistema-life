<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
class UsersController extends Controller
{
	public function index()
	{
    	return view("users");

	}
	public function getUsers(){
		return Response(User::all(),200);
	}
	public function saveUser(Request $request){
		$picture = $request->input('picture');
		$name=$request->input("name");
		$user=$request->input("username");
		$mail=$request->input("email");
		$pass=$request->input("password");
		$address=$request->input("address");
		$tel = $request->input("phone");
		$starttime = $request->input("startDate");
		$position= $request->input("cargo");
		$status= $request->input("status");
		$note= $request->input("note");
		$roles= $request->input("rol");
		$data = new User();
		if ($picture)
		{	
			$url = $this->export_image('users/'.$user, $picture);
			$data->url_img=$url;
		}
		$data->name=$name;
		$data->username=$user;
		$data->email=$mail;
		$data->direccion=$address;
		$data->telefono=$tel;
		$data->fecha_ingreso=$starttime;
		$data->cargo=$position;
		$data->status= $status;
		$data->nota_baja = $note? $note : null;
		$data->password=\Hash::make($pass);
		$data->save();
		foreach ($roles as $key => $value) {
			DB::table("users_roles")->insert(['id_user'=> $data->id, 'id_rol'=>$value]);
		}
		return response(["result"=>"ok", "response" => "usuario creado Exitosamente!"],200);
	}
	public function updateUser(Request $request){
		$picture = $request->input('picture');
		$name=$request->input("name");
		$user=$request->input("username");
		$mail=$request->input("email");
		$pass=$request->input("password");
		$address=$request->input("address");
		$tel = $request->input("phone");
		$starttime = $request->input("startDate");
		$position= $request->input("cargo");	
		$status= $request->input("status");
		$note= $request->input("note");
		$data = User::find($request->input("userID"));
		$roles= $request->input("rol");
	//	dd($roles);
		$data->name=$name;
		$data->username=$user;
		$data->email=$mail;
		$data->direccion=$address;
		$data->telefono=$tel;
		$data->fecha_ingreso=$starttime;
		$data->cargo=$position;
		$data->status= $status;
		$data->nota_baja = $note? $note : null;
		if ($picture && $picture != $data->url_img )
		{	
			$url = $this->export_image('users/'.$user, $picture);
			$data->url_img=$url;
		}
		if($pass != "")
			$data->password=\Hash::make($pass);
		$data->save();
		DB::table("users_roles")->where("id_user", $data->id)->delete();
		foreach ($roles as $key => $value) {
			DB::table("users_roles")->insert(['id_user'=> $data->id, 'id_rol'=>$value]);
		}
		return response(["result"=>"ok", "response" => "usuario actualizado Exitosamente!"],200);
	}
}
