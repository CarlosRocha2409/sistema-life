<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
         session(['id_user'=> Auth::User()->id]);
        session(['username'=> Auth::User()->username]);
        session(['name'=> Auth::User()->username]);
        $html=Controller::GetMenu();
        session(['menu'=>$html]);
        return view('home');
    }
}
