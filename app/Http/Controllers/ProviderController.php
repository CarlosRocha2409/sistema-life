<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Auth;

class ProviderController extends Controller
{
    public function index(Request $request){
    	session(['id_user'=> Auth::User()->id]);
        session(['username'=> Auth::User()->username]);
        session(['name'=> Auth::User()->username]);
        $html=Controller::GetMenu();
        session(['menu'=>$html]);
        $var=Controller::GetFormOptions(session('id_user'),$request->path());
    	return view("Providers",['options'=>$var]);
    }
    public function saveProvider(Request $rq){
    	$data= $rq->all();
    	$data["id_user_created"]=session("id_user");
    	$data["date_created"]=Carbon::now();
    	DB::table("proveedores")->insert($data);
    	return response(["resultado"=>"OK"],200);
    }
    public function updateProvider(Request $rq){
    	$data= $rq->except("id_proveedor");
    	$data["id_user_updated"]=session("id_user");
    	$data["date_updated"]=Carbon::now();
    	DB::table("proveedores")->where("id_proveedor", $rq->input("id_proveedor"))->update($data);
    	return response(["resultado"=>"OK"],200);
    }
    public function GetProviders(){
    	return response(DB::table("proveedores")->get(),200);
    }
}
