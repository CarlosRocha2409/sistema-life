<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class BillingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
    	session(['id_user'=> Auth::User()->id]);
        session(['username'=> Auth::User()->username]);
        session(['name'=> Auth::User()->username]);
        $html=Controller::GetMenu();
        session(['menu'=>$html]);
        $var=Controller::GetFormOptions(session('id_user'),$request->path());
        return view('Billing',['options'=>$var]);
    }
}
