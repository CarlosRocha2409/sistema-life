<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function GetMenu()
    {
    	$data=DB::select('select distinct(f.id_form), f.id_father_form, f.form, f.file_name, f.Level from 
		users u inner join users_roles ur on u.id = ur.id_user
		inner join roles r on r.id_rol = ur.id_rol inner join roles_forms rf on r.id_rol = rf.id_rol
		inner join forms f on f.id_form = rf.id_form where u.id = :user and f.level = 1 and f.version= 2',['user'=>session('id_user')]);
    	$html='';
    	foreach ($data as $key => $value) 
    	{
			$html.='<li class="treeview"><a href="#">';  	
            $html.='<span> '.$value->form.' </span>';
            $html.='<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a><ul class="treeview-menu">';
            
          	$submenu=DB::select('select f.* from users u inner join users_roles ur on u.id = ur.id_user inner join roles r on 
						r.id_rol = ur.id_rol inner join roles_forms rf on r.id_rol = rf.id_rol inner join forms f on f.id_form = rf.id_form 
						where u.id = :user and f.level = 2 and f.id_father_form = :father and f.version=2',["user"=>session('id_user'), "father"=>$value->id_form]); 
			foreach ($submenu as $index => $valor) 
			{
				$html.='<li><a href="'.$valor->file_name.'"><i class="fa fa-circle-o"></i>'.$valor->form.'</a></li>';
	    	}    	
    		$html.='</ul></li>';
    	}
		session(['menu',$html]);
		return $html;
    }
    public function truncate($value)
    {
    	$string=(string)$value;
    	$xploded=explode('.', $string);
    	if(array_key_exists(1, $xploded))
    	{
 		   	return floatval($xploded[0].'.'.substr($xploded[1],0,2));
    	}
    	else
    	{
    		return $value;
    	}
    }

    function export_image($path, $img)
	{

		try
		{
			if(strlen($img)>0)
			{
				if(strpos($img, "base64") !== false){
					$directory = $this->create_directory($path);
					$data = str_replace('data:image/png;base64,', '', str_replace('data:image/jpeg;base64,', '', $img));
					$imageData = base64_decode($data);
					$source = imagecreatefromstring($imageData);
					$rotate = imagerotate($source, 360, 0);
					$imageName = $path.'/'.date('dmYHis').'.jpeg';
					$imageSave = imagejpeg($rotate, $imageName, 100);
					imagedestroy($source);
				}else{
					return $img;
				}
				
			}
			else
			{
				return null;
			}
		
			return $imageName;
		} 
		catch (Exception $e) 
		{
			return null;
		}

	}
	public function GetFormOptions($id_user, $form_name)
	{
		
		$id_form=DB::table('forms')->where('file_name',$form_name)->first();
		if($id_form)
		{
			return DB::select('select fo.* from users u inner join users_roles ur on u.id = ur.id_user inner join roles r 
	        on r.id_rol = ur.id_rol inner join roles_forms rf on r.id_rol = rf.id_rol inner join forms f on f.id_form = rf.id_form  
	        inner join roles_forms_options rfo on f.id_form = rfo.id_form and r.id_rol = rfo.id_rol inner join forms_options fo on rfo.id_option = fo.id_option 
	        where u.id = :user and f.level = 2 and f.id_form = :form',['user'=>$id_user, 'form'=>$id_form->id_form]);
		}
		else
		{
			return array();
		}
	}
	public function getRoles()
	{
 		return response(DB::table('roles')->where('status',1)->get(),200);
	}
	public function getUserRol(Request $rq)
	{
		return response(DB::table('users_roles')->where('id_user',$rq->input("userID"))->get(),200);
	}
	public function getProviders(){
		return response(DB::select("select id_proveedor, concat(codigo_proveedor, ' - ', nombre_comercial) proveedor from proveedores where id_status=1;"),200);
	}
	public function saveCustomerBilling(Request $rq){
		$data=$rq->all();
		$data["id_user_created"]=session("id_user");
    	$data["date_created"]=Carbon::now();
		DB::table("clientes")->insert($data);
    	return response(["resultado"=>"OK"],200);
	}
	public function getCustomers(){
		return response(DB::table("clientes")->where("id_status",1)->get(),200);
	}
	function create_directory($path)
	{
		if(!file_exists($path))
		{
			$result = mkdir($path, 0777, true);
		}
		else
		{
			$result = false;
		}
		
		return $result;
	}
}
